﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

public class GetCardCollectionRequestBuilder : RestRequestBuilder
{

    public GetCardCollectionRequestBuilder(string requestMethod) : base(requestMethod)
    {
        var adressBuilder = buildDataServerUserUrl(UserInfo.UserName);
        adressBuilder.Append(UserInfo.CardsCollectionTail);
        WebRequest.url = adressBuilder.ToString();
        addAuthX_TokenHeader();
        addDownloadHandler();
    }
}
