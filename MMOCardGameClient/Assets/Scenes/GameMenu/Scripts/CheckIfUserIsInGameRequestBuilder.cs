﻿using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class CheckIfUserIsInGameRequestBuilder : RestRequestBuilder
{
    public CheckIfUserIsInGameRequestBuilder(bool userIsInGame = false, string requestMethod = UnityWebRequest.kHttpVerbGET) : 
        base(requestMethod)
    {
        var adressBuilder = buildDataServerUserUrl(UserInfo.UserName);
        adressBuilder.Append(userIsInGame ? UserInfo.GameUrlTail : UserInfo.CheckIfUserIsInGameTail);
        WebRequest.url = adressBuilder.ToString();
        addAuthX_TokenHeader();
        addDownloadHandler();
    }
	
}
