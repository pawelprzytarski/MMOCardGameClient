﻿using System;
using System.Collections;
using System.Collections.Generic;
using Backend.GameInformation;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using ExtensionMethods;
using CardCosts = ProtoBuffers.CardCosts; //TODO: remove this line, if it is unused

public class WaitForGameScript : MonoBehaviour
{
    public GameObject WaitingPanel;
    public Text UserInfoText;
    public Button CancelSearchButton;

    public ConnectToBattleServerScript ConnectToBattleServer;

    private void displayExceptionMessage(Exception e)
    {
        UserInfoText.text = e.Message;
        UserInfoText.color = Color.red;
    }

    public void cancelQueueing()
    {
        StopCoroutine(WaitForGameAsync());
        WaitingPanel.SetActive(false);
    }


    public void waitForGame()
    {
        if (UserInfo.SearchState == SearchState.WAITING_FOR_QUEUE) Debug.Log("Waiting for game..."); 
        this.StartCoroutine(WaitForGameAsync(), displayExceptionMessage);
        CancelSearchButton.onClick.AddListener(cancelQueueing);
    }

    public IEnumerator WaitForGameAsync()
    {
        while (UserInfo.SearchState == SearchState.WAITING_FOR_QUEUE)
        {
            var requestBuilder = new CheckIfUserIsInGameRequestBuilder();
            yield return requestBuilder.WebRequest.SendWebRequest();

            if (checkIfUserIsInGame(getWaitingResponse(requestBuilder))) yield break;

            yield return new WaitForSeconds(1);
        }
    }

    private static DataServerResponse getWaitingResponse(CheckIfUserIsInGameRequestBuilder requestBuilder)
    {
        return new DataServerResponse.DataServerResponseBuilder(requestBuilder.WebRequest, "WaitForGame")
            .addResponseCodeMessage(HttpCode.Ok, string.Empty, true)
            .addResponseCodeMessage(HttpCode.Forbidden, UserInfo.getLocalizedString("UserDoesNotExist"), false)
            .build();
    }

    private bool checkIfUserIsInGame(DataServerResponse response)
    {
        if (!response.Success)
        {
            response.getMessageForUser(ref UserInfoText);
            return true;
        }
        else
        {
            if (!response.DownloadedText.Contains(@"true")) return false;
            UserInfo.SearchState = SearchState.WAITING_FOR_SERVER_ADRESS;
            this.StartCoroutine(waitForServerAdressAsync(), displayExceptionMessage);
        }

        return false;
    }


    public IEnumerator waitForServerAdressAsync()
    {
        if (UserInfo.SearchState != SearchState.WAITING_FOR_SERVER_ADRESS) yield break;
        var requestBuilder = new CheckIfUserIsInGameRequestBuilder(true);
        yield return requestBuilder.WebRequest.SendWebRequest();

        var response = getJoiningResponse(requestBuilder);
        response.getMessageForUser(ref UserInfoText);

        if (checkIfAnErrorOcurred(response)) yield break;
        
        GameInfo.Token = JsonConvert.DeserializeObject<UserGamePlayInfo>(response.DownloadedText)?.authToken;
        ConnectToBattleServer.ConnectToBattleServer();
    }

    private static DataServerResponse getJoiningResponse(CheckIfUserIsInGameRequestBuilder requestBuilder)
    {
        return new DataServerResponse.DataServerResponseBuilder(requestBuilder.WebRequest, "JoinGame")
            .addResponseCodeMessage(HttpCode.Ok, UserInfo.getLocalizedString("JoiningMatch"), true)
            .addResponseCodeMessage(HttpCode.Forbidden, UserInfo.getLocalizedString("UserDoesNotExist"), false)
            .addResponseCodeMessage(HttpCode.BadRequest, UserInfo.getLocalizedString("PlayerIsNotInGame"), false)
            .build();
    }

    private bool checkIfAnErrorOcurred(DataServerResponse response)
    {
        if (response.ResponseCode == (long)HttpCode.NotFound)
        {
            UserInfo.SearchState = SearchState.WAITING_FOR_QUEUE;
            this.StartCoroutine(WaitForGameAsync(), displayExceptionMessage);
            return true;
        }
        else if (response.ResponseCode == (long)HttpCode.BadRequest || !response.Success)
        {
            UserInfo.SearchState = SearchState.NOT_SEARCHING;
            return true;
        }

        return false;
    }
}

public class GameInfoJson
{
    public string gamePlayId;
    public string login;
    public IList<string> logins;
    public string gamePlayType;
    public long startDate;
}

public class UserGamePlayInfo {
    public string gamePlayId;
    public string login;
    public string authToken;
    public IList<string> logins;
    public string gamePlayType;
    public string startDate;
    public string gamePlayState;
    public int sessionId;
}

public enum GamePlayTypes
{
    TWO_PLAYERS, THREE_PLAYERS, FOUR_PLAYERS, TEAM_GAME
}