﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeadMenuPanelButtonBehaviorWhileSearching : MonoBehaviour 
{
	public Button StoreButton;
	public Button CollectionButton;
	public Button DeckButton;
	public Button PackagesButton;
	public Button LogoutButton;
	public Button SettingsButton; 
	public Button PreviousButton;

	public SearchForGameScript SearchForGameScript;
	
	// Use this for initialization
	void Start () {
		LogoutButton.onClick.AddListener(SearchForGameScript.cancelSearch);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (UserInfo.SearchState != SearchState.NOT_SEARCHING)
			disableButtons();
		else
			disableButtons(false);
	}

	private void disableButtons(bool disable = true)
	{
		CollectionButton.interactable = !disable;
		DeckButton.interactable = !disable;
		PreviousButton.interactable = !disable;
	}
}
