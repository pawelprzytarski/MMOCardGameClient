﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UserDeckJSON
{
    public int id;
    public string name;
    public IList<UserCardCollection> cardsWithAmounts;
}
