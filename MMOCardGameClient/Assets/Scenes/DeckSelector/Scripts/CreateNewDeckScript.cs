﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CreateNewDeckScript : MonoBehaviour
{
    public InputField DeckNameInputField;
    public Button OkButton;
    public Button CancelButton;
    public GameObject CreateNewDeckPanel;
    public Text InfoText;
    public Text TitleText;
    public UserDecks Decks;
    public SelectionBehaviour SelectionBehaviour;

    public UserDeckCloing DeckCloing;
    private string currentClondedDeckName;
    private string newDeckName;

    private bool allNecessaryComponentsHaveBeenInitialized => DeckNameInputField != null && OkButton != null &&
                                                              CancelButton != null && CreateNewDeckPanel != null &&
                                                              Decks != null && InfoText != null && TitleText != null &&
                                                              SelectionBehaviour != null;

    private Action<DataClient.Void> SuccessCallback;
    
    void Start()
    {
        if(!allNecessaryComponentsHaveBeenInitialized)
            Debug.LogError("CreateNewDeckScript in \"" + gameObject.GetComponent<RectTransform>().name + 
                           "\": Not all components have been initialized!");
    }

    private void Update()
    {
        OkButton.interactable = !string.IsNullOrEmpty(DeckNameInputField.text);
    }

    public void ActivatePanelForNewDeckCreation()
    {
        CreateNewDeckPanel.SetActive(true);
        TitleText.text = UserInfo.getLocalizedString("Create new deck");
        OkButton.GetComponentInChildren<Text>().text = UserInfo.getLocalizedString("Create");
        OkButton.onClick.RemoveAllListeners();
        OkButton.onClick.AddListener(CreateNewDeck);
        
        SuccessCallback = obj =>
        {
            CloseDeckCreationWindow();
            SceneManager.LoadSceneAsync("DeckEditor");
        };
    }

    public void ActivatePanelForDeckClonig()
    {
        CreateNewDeckPanel.SetActive(true);
        TitleText.text = UserInfo.getLocalizedString("Clone deck");
        OkButton.GetComponentInChildren<Text>().text = UserInfo.getLocalizedString("Clone");
        OkButton.onClick.RemoveAllListeners();
        OkButton.onClick.AddListener(DeckCloing.CloneSelectedDeck);
        currentClondedDeckName = SelectionBehaviour.SelectedDecks[0].DeckName;
        
        SuccessCallback = obj =>
        {
            CloseDeckCreationWindow();
            DeckCloing.AddCardsToClonedDeckCallback(currentClondedDeckName, newDeckName);
        };
    }

    public void CreateNewDeck()
    {
        if (deckAlreadyExists()) return;
        UserInfo.DeckToEdit = newDeckName = DeckNameInputField.text;
        var request = UserInfo.LoggedClient.createNewDeck(newDeckName);
        request.SuccessCallback += SuccessCallback;
        request.ErrorCallback += obj =>
        {
            foreach (var error in obj)
            {
                Debug.Log(error);
            }

            InfoText.text = UserInfo.getLocalizedString("UnknownError");
        };
        request.Send();
    }

    private bool deckAlreadyExists()
    {
        if (!Decks.ContainsDeckWithName(DeckNameInputField.text)) return false;
        InfoText.text = UserInfo.getLocalizedString("Deck already exists!");
        return true;

    }

    public void CloseDeckCreationWindow()
    {
        DeckNameInputField.text = string.Empty;
        InfoText.text = string.Empty;
        CreateNewDeckPanel.SetActive(false);
    }
    
    
}
