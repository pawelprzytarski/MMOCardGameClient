﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public class UserDecks : MonoBehaviour
{
	public GameObject LoadingPanel;
	public RectTransform DeckZone;
	public GameObject DeckPrefab;
	public CanvasGroup ButtonCanvasGroup;

	private Dictionary<string, UserDeckJSON> decks = new Dictionary<string, UserDeckJSON>();
	private bool allNecessaryFieldsInitialized => DeckPrefab != null && LoadingPanel != null && DeckZone != null &&
	                                              ButtonCanvasGroup != null;

	void Start()
	{
		if (!allNecessaryFieldsInitialized)
			Debug.LogError("UserDecks script in \"" + gameObject.GetComponentInParent<RectTransform>().name +
			               "\" is not properly initialized!");
		else
			GetUserDecks();
	}

	public void GetUserDecks()
	{
		destroyDeckObjects();
		ButtonCanvasGroup.interactable = false;
		LoadingPanel.GetComponent<LoadingPanelBehaviour>().disable = false;
		LoadingPanel.SetActive(true);
		getUserDecks();
	}
	
	private void getUserDecks()
	{
		var request = UserInfo.LoggedClient.getUserDecks();
		request.SuccessCallback += displayDecks;
		request.ErrorCallback += obj =>
		{
			foreach (var error in obj)
			{
				Debug.Log(error);
			}
		};
		request.Send();
	}

	private void displayDecks(UserDeckJSON[] userDecksJson)
	{
		decks.Clear();
		foreach (var deck in userDecksJson)
		{
			createAndDisplayDeck(deck);
			try
			{
				decks.Add(deck.name, deck);
			}
			catch (Exception e)
			{
				//ignore
			}
		}

		LoadingPanel.GetComponent<LoadingPanelBehaviour>().disable = true;
		ButtonCanvasGroup.interactable = true;

	}

	private void createAndDisplayDeck(UserDeckJSON deck)
	{
		try
		{
			var deckScriptable = DeckCreator.CreateDeck(string.IsNullOrEmpty(deck.name) ? 
					UserInfo.getLocalizedString("Unnamed deck") : deck.name,
				deck.cardsWithAmounts.Sum(cardsWithAmount => int.Parse(cardsWithAmount.amount)));
			var instantiatedDeck = Instantiate(DeckPrefab);
			instantiatedDeck.GetComponent<IPlayfieldItemConfiguration>()?.setUp(deckScriptable);
			instantiatedDeck.GetComponent<RectTransform>().SetParent(DeckZone);
		}
		catch (Exception e)
		{
			Debug.LogError("Unable to create deck: " + deck.name + ".\n" + e.Message);
		}
		
	}

	public bool ContainsDeckWithName(string name)
	{
		return decks.ContainsKey(name);
	}

	public UserDeckJSON GetDeck(string name)
	{
		return decks[name];
	}


	private void destroyDeckObjects()
	{
		foreach (Transform child in DeckZone)
		{
			Destroy(child.gameObject);
		}
	}
}
