﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarScript : MonoBehaviour
{
    private PlayerInfos playerInfos = null;
    [SerializeField] private Text label = null;
    [SerializeField] private StatusBarLayout healthBarControl;
    private int currentLife = 0;
    private const float MAX_LIFE = GameConstants.BASE_PLAYER_LIFE * 1.0f;
    private bool changed = true;

    void Start()
    {
        try
        {
            playerInfos = GetComponentInParent<PlayerInfos>();
            CallbackListeners.get().PlayerLifeChangedEvent += OnPlayerLifeChangedEvent;
            currentLife = GameConstants.BASE_PLAYER_LIFE;
            if(healthBarControl == null) throw new NullReferenceException("healthBarContol not initialized!");
        }
        catch (NullReferenceException e)
        {
            Debug.Log("Something is strange on this Scene" + e.Message);
        }
    }

    private void OnPlayerLifeChangedEvent(int playerid, int newlife)
    {
        if (playerid == playerInfos.playerId)
        {
            currentLife = newlife;
            changed = true;
        }
    }

    void Update()
    {
        if (healthBarControl == null || !changed)
            return;
        changed = false;
        var life = currentLife; //because multitasking
        healthBarControl.VisiblePercentage = Mathf.Floor((life / MAX_LIFE) * 100);
        if (label != null)
            label.text = life.ToString();
    }
}