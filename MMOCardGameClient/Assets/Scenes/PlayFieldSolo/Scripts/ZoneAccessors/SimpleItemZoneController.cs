﻿using System;
using System.Collections.Generic;
using Backend.GameInformation;
using ProtoBuffers;
using UnityEngine;

public class SimpleItemZoneController : MonoBehaviour, ISimpleItemZoneController
{
	public delegate void AddedObject(GameObject gameObject);
	
	public RectTransform ItemZone;

	public AddedObject AddedObjectMethod=null;

	public GameObject ItemPrefab;
	private GameObject instantiatedItem;

	public Vector2 PreferredSize;

	private RectTransform itemRectTransform;
	private IPlayfieldItemConfiguration itemConfiguration;
	
	private Dictionary<int, GameObject> _itemsDictionary=new Dictionary<int, GameObject>();

	void Start()
	{
		FindZoneIfNotSet();
	}

	protected internal void FindZoneIfNotSet()
	{
		if (ItemZone == null) ItemZone = GetComponent<RectTransform>();
	}

	public Dictionary<int, GameObject>.KeyCollection GetKeys()
	{
		return _itemsDictionary.Keys;
	}

	public GameObject AddNewCardToScene(Card card)
	{
		var cardInfo = CardCreator.createNewCard(card);
		return AddItemToScene(cardInfo, card.InstanceId);
	}

	public GameObject AddItemToScene(ScriptableObject itemToAdd, int id)
	{
		try
		{
			var item=addItem(itemToAdd, id);
			AddedObjectMethod?.Invoke(item);
			return item;
		}
		catch (Exception e)
		{
			displayError(string.Concat("Unable to add ", instantiatedItem.name, " to scene - ", e.Message), 
				"SimpleItemZoneController - AddItemToScene", false);
			return null;
		}
	}

	public void AddItemToScene(GameObject existingObject, int id)
	{
		if (_itemsDictionary.ContainsKey(id))
		{
			displayError(
				string.Concat("Unable to add object with id", id, " - object already exists in ", ItemZone.name), 
				"SimpleItemZoneController - AddItemToScene", false);
		}
		_itemsDictionary.Add(id, existingObject);
		SetParentForGameObject(existingObject);
		AddedObjectMethod?.Invoke(existingObject);
	}

	private void SetParentForGameObject(GameObject existingObject)
	{
		var movableItem = existingObject.GetComponent<IMovable>();
		if (movableItem != null)
			movableItem.SetParent(ItemZone);
		else existingObject.transform.SetParent(ItemZone);
	}

	private GameObject addItem(ScriptableObject itemToAdd, int id)
	{
		instantiatedItem = Instantiate(ItemPrefab);
		getNecessaryItemComponents();
		configureItemComponents(itemToAdd);
		SetParentForGameObject(instantiatedItem);
		
		_itemsDictionary.Add(id, instantiatedItem);
		
		return instantiatedItem;
	}

	private void getNecessaryItemComponents()
	{
		itemConfiguration = instantiatedItem.GetComponent<IPlayfieldItemConfiguration>();
		itemRectTransform = instantiatedItem.GetComponent<RectTransform>();
	}

	private void configureItemComponents(ScriptableObject itemToAdd)
	{
		if (PreferredSize != Vector2.zero) 
			itemRectTransform.sizeDelta = PreferredSize;
		
		if(itemConfiguration != null)
			itemConfiguration.setUp(itemToAdd);
		else
			Debug.LogWarning("SimpleItemZoneController: " + instantiatedItem.name + 
			                 " has no configuration script. It will be added uncofigured");
	}

	public GameObject GetItemById(int id)
	{
		if (_itemsDictionary.ContainsKey(id)) return _itemsDictionary[id];
		var message = "Can't find item with id " + id;
		displayError(message, "SimpleItemZoneController - GetItemById", true);
		throw new Exception(message);
	}

	public void UpdateItemWithId(ScriptableObject updatedItem, int id)
	{
		if (_itemsDictionary.ContainsKey(id))
			_itemsDictionary[id].GetComponent<IPlayfieldItemConfiguration>().setUp(updatedItem);
		else
			displayError("Can't find item with id" + id, "SimpleItemZoneController - UpdateItemWithId", false);
	}

	public bool HasItem(int id)
	{
		return _itemsDictionary.ContainsKey(id);
	}

	public GameObject RemoveItem(int id)
	{
		var removedItem = GetItemById(id);
		_itemsDictionary.Remove(id);
		return removedItem;
	}
	
	public void DestroyItem(int id)
	{
		if (!_itemsDictionary.ContainsKey(id))
		{
			Debug.LogWarning("SimpleItemZoneController: Tied to remove non existing item with id " + 
			                 id + " in zone " + ItemZone.name);
			return;
		}

		Destroy(_itemsDictionary[id]);
		_itemsDictionary.Remove(id);
	}

	public void ClearZone()
	{
		foreach (RectTransform item in ItemZone)
		{
			Destroy(item.gameObject);
		}
		_itemsDictionary.Clear();
	}

	private void displayError(string errorMessage, string title, bool severeError)
	{
		Debug.LogWarning(errorMessage);
		CallbackListeners.get().OnClientSevereExceptionEvent(errorMessage, 
			string.Concat("SimpleItemZoneContoller for ", ItemZone.name), severeError);
	}
}
