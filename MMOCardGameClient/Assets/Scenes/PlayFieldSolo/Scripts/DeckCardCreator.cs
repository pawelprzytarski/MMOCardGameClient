﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Backend.GameInformation;
using Backend.GameInformation.Enums;
using ProtoBuffers;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeckCardCreator : MonoBehaviour
{
	[SerializeField] private CardZoneController deckController;
	private RectTransform deckControllerObject;
	[SerializeField] private PlayfieldCardsCollection CardsCollection;
	private PlayerInfos playerInfos;
	[SerializeField] private ReactionStackController ReactionStackController;
	[SerializeField] private AttackDefenderTargetSelector targetSelector;
	
	public DeckCardCreator()
	{
		CallbackListeners.get().CardRevealedEvent+=OnCardRevealedEvent;
		CallbackListeners.get().CardMovedEvent+=OnCardMovedEvent;
		CallbackListeners.get().CardCreatedEvent+=OnCardCreatedEvent;
	}

	private void OnCardCreatedEvent(Card card, int owner, CardPosition position)
	{
		Card cardCopy = new Card(card.InstanceId);
		if (owner != playerInfos.playerId)
			return; 
		deckControllerObject = deckController.GetComponent<RectTransform>();
		var controllerPosition=deckControllerObject.position;
		var controllerSize=deckControllerObject.sizeDelta;
		
		playerInfos = GetComponentInParent<PlayerInfos>();

		var cardScriptable = CardCreator.createNewCard(cardCopy);
		var cardChild = deckController.AddItemToScene(cardScriptable, 0);
		SetCardToStartPosition(0, controllerPosition, controllerSize);
			
		var movableCard = cardChild.GetComponent<MovableCard>();
		if (movableCard != null && ReactionStackController != null)
			movableCard.DoubleClick?.AddListener(ReactionStackController.CardPlay);
		if (movableCard != null && targetSelector != null)
			movableCard.Click?.AddListener(targetSelector.ToggleCardSelect);
		var cardDisplay = cardChild.GetComponent<CardDisplay>();
		cardDisplay.setUp(cardScriptable);
		cardDisplay.ChangeToPlayfieldCard();

		if (position!=CardPosition.Library)
		{
			var gameObject = deckController.RemoveItem(0);
			CardsCollection.addCard(cardCopy.InstanceId, gameObject);
			CallbackListeners.get().OnCardMoved(cardCopy, owner, CardPosition.Library, owner, position);
		}

	}

	private void OnCardRevealedEvent(Card card, int owner, CardPosition position)
	{
		if (CardsCollection == null
		    || owner!=playerInfos.playerId) return;
		if (!CardsCollection.HasCard(card.InstanceId))
		{
			var cardScriptable = CardCreator.createNewCard(card);
			int childId=-1;
			Transform child=null;
			CardDisplay cardChild=null;


			if (card.PositionIndex >= 0)
			{
				childId = (int) card.PositionIndex;
				child = deckControllerObject.GetChild(childId);
				cardChild = child.GetComponent<CardDisplay>();
			}
			else findFreeCard(ref childId, ref child, ref cardChild);
			
			childId = cardChild.InstanceId;

			deckController.UpdateItemWithId(cardScriptable,childId);
			var gameObject = deckController.RemoveItem(childId);
			
			deckController.AddItemToScene(gameObject, card.InstanceId);
			if(card.PositionIndex>=0)
				gameObject.GetComponent<Transform>().SetSiblingIndex((int) card.PositionIndex);
			
			CardsCollection.addCard(card.InstanceId, gameObject);
		}
	}

	private void OnCardMovedEvent(Card card, int oldowner, CardPosition oldposition, int newonwer, CardPosition newposition)
	{
		if (CardsCollection == null
		    || oldowner!=playerInfos.playerId) 
			return;
		if (!CardsCollection.HasCard(card.InstanceId))
		{
			var cardScriptable = CardCreator.createNewCard(card);
			int childId=-1;
			Transform child=null;
			CardDisplay cardChild=null;

			findFreeCard(ref childId, ref child, ref cardChild);

			if (cardChild == null)
			{
				CallbackListeners.get()
					.OnClientSevereExceptionEvent("Not enough free cards for player: " + playerInfos.playerId);
				return;
			}

			childId = cardChild.InstanceId;

			deckController.UpdateItemWithId(cardScriptable,childId);
			var gameObject = deckController.RemoveItem(childId);
			
			CardsCollection.addCard(card.InstanceId, gameObject);
		}
	}

	void findFreeCard(ref int childId, ref Transform child, ref CardDisplay cardChild)
	{
		for (int i = deckControllerObject.childCount - 1; i >= 0; i--)
		{
			childId = i;
			child = deckControllerObject.GetChild(childId);
			cardChild = child.GetComponent<CardDisplay>();
			if (cardChild.InstanceId < 0)
				return;
		}

		childId = -1;
		child = null;
		cardChild = null;
	}

	void Start ()
	{
		if(CardsCollection == null)
			Debug.Log("DeckCardCreator: PlayfieldCardsCollection not initlialized!");
		/*
		// to tu zostaje, bo nigdy nie wiadomo, kiedy karty znowu zaczną płonąć 
		// a to pomaga w ich ogarnianiu bez konieczności łączenia się z serwerem
		GameInfo.GameState=new GameState("user");
		GameInfo.GameState.AddPlayer("user", 0, 0, 20,41);
		GameInfo.GameState.AddPlayer("user1", 1, 0, 20,41);
		GameInfo.GameState.FindMyPlayerId();/**/
		
		deckControllerObject = deckController.GetComponent<RectTransform>();
		var controllerPosition=deckControllerObject.position;
		var controllerSize=deckControllerObject.sizeDelta;
		
		playerInfos = GetComponentInParent<PlayerInfos>();
		int cardsCount = GameInfo.GameState.GetPlayer(playerInfos.playerId).CardsInLibraryCount;

		for (int i = 0; i < cardsCount; i++)
		{
			deckController.AddItemToScene(CardCreator.createNewCard(new Card(-i-1)), -i-1);
			SetCardToStartPosition(i, controllerPosition, controllerSize);
			
			var movableCard = deckController.GetItemById(-i-1).GetComponent<MovableCard>();
			if (movableCard != null && ReactionStackController != null)
				movableCard.DoubleClick?.AddListener(ReactionStackController.CardPlay);
			if (movableCard != null && targetSelector != null)
				movableCard.Click?.AddListener(targetSelector.ToggleCardSelect);
		}
		
	}

	private void SetCardToStartPosition(int i, Vector3 controllerPosition, Vector2 controllerSize)
	{
		var card = deckController.GetItemById(-i-1).GetComponent<RectTransform>();
		card.position = controllerPosition;
		for (int j = 0; j < card.childCount; j++)
		{
			var child = card.GetChild(j).GetComponent<RectTransform>();
			child.position = controllerPosition;
			child.sizeDelta = controllerSize;
			for (int k = 0; k < child.childCount; k++)
			{
				var grandchild = child.GetChild(k).GetComponent<RectTransform>();
				grandchild.position = controllerPosition;
				grandchild.sizeDelta = new Vector2();
			}
		}
	}
}
