﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardInfoScript : MonoBehaviour
{
	[SerializeField] private PlayfieldCardsCollection CardsCollection;
	private RectTransform RectTransform;
	private GameObject currentCard=null;

	void Start()
	{
		RectTransform = GetComponent<RectTransform>();
		CardsCollection.AddedCard.AddListener(NewCardAdded);
	}

	private void NewCardAdded(GameObject arg0)
	{
		arg0.GetComponent<MovableCard>().Click.AddListener(CardClick);
	}

	private void CardClick(ClickData arg0)
	{
		if (arg0.Button == PointerEventData.InputButton.Right)
		{
			if(currentCard!=null)
				DestroyCurrentCard();
			var newCard = Instantiate(arg0.Card);
			newCard.transform.SetParent(transform);
			var movableCard = newCard.GetComponent<MovableCard>();
			movableCard.Click.AddListener(CardInfoClick);
			movableCard.AnimationMoveSpeed = 100000000;
			movableCard.AnimationResizeSpeed = 1000000000;
			newCard.ChangeToFullCard();
			var rectTransform = newCard.GetComponent<RectTransform>();
			rectTransform.sizeDelta = RectTransform.sizeDelta;
			rectTransform.position = new Vector3(transform.position.x+rectTransform.sizeDelta.x/2, transform.position.y);
			currentCard = newCard.gameObject;
		}
	}

	private void CardInfoClick(ClickData arg0)
	{
		DestroyCurrentCard();
	}

	private void DestroyCurrentCard()
	{
		try
		{
			Destroy(currentCard);
		}
		catch (Exception e)
		{
		}

		currentCard = null;
	}
}
