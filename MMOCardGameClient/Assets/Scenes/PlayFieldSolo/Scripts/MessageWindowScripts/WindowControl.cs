﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

public class WindowControl : MonoBehaviour
{
	[SerializeField] private GameObject WindowPrefab;
	[SerializeField] private Canvas MainCanvas;

	void Start()
	{
		CallbackListeners.get().SevereExceptionEvent += createNewWindow;
	}

	public void createNewWindow(string message, string title = null, bool shutdownAfterClosingWindow = false)
	{
		try
		{
			var windowInstance = Instantiate(WindowPrefab);

			var windowConfigScript = windowInstance.GetComponent<ErrorWindowScript>();
			windowConfigScript.DisplayErrorMessage(message, title);
			if (shutdownAfterClosingWindow) windowConfigScript.ShutDownApplicationAfterClosing();

			var rectTransform = windowInstance.GetComponent<RectTransform>();
			rectTransform.position = new Vector2(Screen.width / 2, Screen.height / 2);
			rectTransform.SetParent(MainCanvas.transform);
		}
		catch (Exception e)
		{
			Debug.LogError(e.Message+"\n\n\n"+e.StackTrace);
			Debug.LogWarning(message+"+"+title);
			//Application.Quit();
		}
	}
}
