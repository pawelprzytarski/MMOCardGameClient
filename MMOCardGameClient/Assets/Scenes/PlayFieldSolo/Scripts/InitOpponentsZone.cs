﻿using UnityEngine;

public class InitOpponentsZone : MonoBehaviour
{
	void Start () {
		
		if (GameInfo.GameState != null)
		{
			bool firstPassed = false;
			var opponentField = transform.GetChild(0);
			foreach (var keyValuePair in GameInfo.GameState.GetPlayers())
			{
				if(keyValuePair.Key==GameInfo.GameState.GetMyUserId())
					continue;

				Transform child;
				if (firstPassed)
					child = Object.Instantiate(opponentField, transform);
				else child = opponentField;

				child.GetComponentInChildren<PlayerInfos>().playerId = keyValuePair.Key;
				firstPassed = true;
			}
		}
	}
}
