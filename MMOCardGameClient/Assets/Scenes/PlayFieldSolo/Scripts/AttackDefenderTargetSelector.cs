﻿using System;
using System.Collections.Generic;
using Backend.GameInformation;
using Backend.GameInformation.Enums;
using ProtoBuffers;
using UnityEngine;
using UnityEngine.EventSystems;

public class AttackDefenderTargetSelector : MonoBehaviour
{
    public TimeSpan attackDisplayTime = new TimeSpan(0, 0, 5);
    [SerializeField] private PlayfieldCardsCollection CardsCollection;
    [SerializeField] private PlayersCollection PlayersCollection;
    [SerializeField] private LineBehaviour LinePrefab;
    [SerializeField] private Color LineColor = new Color(255, 255, 0, 255);
    [SerializeField] private Color AttackLineColor = new Color(255, 0, 0, 255);
    [SerializeField] private float LineWidth = 10;
    [SerializeField] private Transform LinesParent;
    Dictionary<int, int> selectedAttackers = new Dictionary<int, int>();
    Dictionary<int, TargetList> selectedTargetList = new Dictionary<int, TargetList>();

    Dictionary<int, List<Tuple<Target, LineBehaviour>>> linesList =
        new Dictionary<int, List<Tuple<Target, LineBehaviour>>>();

    void Start()
    {
        CallbackListeners.get().FinishedChoosingTargetsEvent += OnFinishedChoosingTargetsEvent;
        CallbackListeners.get().AttackersSelectedEvent += OnAttackersSelectedEvent;
        CallbackListeners.get().AttackExecutedEvent += OnAttackExecutedEvent;
        CallbackListeners.get().CardMovedEvent += OnCardMovedEvent;
        CallbackListeners.get().PhaseEndedEvent+=OnPhaseEndedEvent;
    }

    private void OnPhaseEndedEvent(GamePhase oldphase, int oldplayerid, GamePhase newphase, int newplayerid)
    {
        if(newphase==GamePhase.EndRound)
            cleanLines();
    }

    private void OnCardMovedEvent(Card card, int oldowner, CardPosition oldposition, int newonwer, CardPosition newposition)
    {
        destroyExistingLinesForAttacker(card.InstanceId);
    }

    private void OnAttackExecutedEvent(int attackerid, int defenderid, TargetType defendertype)
    {
        if (linesList.ContainsKey(attackerid))
        {
            foreach (var tuple in linesList[attackerid])
            {
                if (tuple.Item1.Id == defenderid && tuple.Item1.TargetType == defendertype)
                    try
                    {
                        Destroy(tuple.Item2.gameObject);
                    }
                    catch (Exception)
                    {
                    }
            }
        }

        var startPos = CardsCollection.getCard(attackerid).transform.position;
        Vector3 destPos = getTargetPosition(defenderid, defendertype);
        var line = CreateLine(startPos, destPos);
        line.Color = AttackLineColor;
        line.DestroyAfter(attackDisplayTime);
    }

    private Vector3 getTargetPosition(int defenderid, TargetType defendertype)
    {
        if (defendertype == TargetType.Card)
            return CardsCollection.getCard(defenderid).transform.position;
        else return PlayersCollection.GetPlayer(defenderid).playerIluminationOverlay.transform.position;
    }

    private void OnAttackersSelectedEvent(Dictionary<int, List<TargetList>> args)
    {
        foreach (var entry in args)
        {
            var attacker = entry.Key;
            var lines = new List<Tuple<Target, LineBehaviour>>();
            var startPos = CardsCollection.getCard(attacker).transform.position;
            createNewLines(entry, lines, startPos);

            destroyExistingLinesForAttacker(attacker);
            linesList[attacker] = lines;
        }
    }

    private void destroyExistingLinesForAttacker(int attacker)
    {
        if (linesList.ContainsKey(attacker))
        {
            foreach (var line in linesList[attacker])
            {
                try
                {
                    Destroy(line.Item2.gameObject);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }
    }

    private void createNewLines(KeyValuePair<int, List<TargetList>> entry, List<Tuple<Target, LineBehaviour>> lines, Vector3 startPos)
    {
        foreach (var targetList in entry.Value)
        {
            foreach (var target in targetList.Targets)
            {
                addLineBetweenAttackerAndTarget(target, lines, startPos);
            }
        }
    }

    private void OnFinishedChoosingTargetsEvent(int transactionid, int instanceid, TargetList targets)
    {
        if (GameInfo.GameState.CurrentPhase != GamePhase.Attack && GameInfo.GameState.CurrentPhase != GamePhase.Defence)
            return;
        selectedAttackers[instanceid] = transactionid;
        selectedTargetList[instanceid] = targets;
        var lines = new List<Tuple<Target, LineBehaviour>>();
        linesList[instanceid] = lines;

        var startPos = CardsCollection.getCard(instanceid).transform.position;
        foreach (var target in targets.Targets)
        {
            addLineBetweenAttackerAndTarget(target, lines, startPos);
        }
    }

    private void addLineBetweenAttackerAndTarget(Target target, List<Tuple<Target, LineBehaviour>> lines,
        Vector3 startPos)
    {
        Vector3 destPos = getTargetPosition(target.Id, target.TargetType);
        var line = CreateLine(startPos, destPos);
        lines.Add(new Tuple<Target, LineBehaviour>(target, line));
    }

    private LineBehaviour CreateLine(Vector3 startPos, Vector3 destPos)
    {
        var line = Instantiate(LinePrefab);
        line.SetStartPosition(startPos);
        line.SetStopPosition(destPos);
        line.Color = LineColor;
        line.Width = LineWidth;
        line.transform.SetParent(LinesParent);
        return line;
    }

    public void ToggleCardSelect(ClickData clickData)
    {
        if (clickData.Button != PointerEventData.InputButton.Left)
            return;
        if (selectedAttackers.ContainsKey(clickData.Card.InstanceId))
        {
            cleanupForCard(clickData);
        }
        else
        {
            startTransactionForNewAttacker(clickData);
        }
    }

    private void cleanupForCard(ClickData clickData)
    {
        selectedAttackers.Remove(clickData.Card.InstanceId);
        selectedTargetList.Remove(clickData.Card.InstanceId);
        foreach (var line in linesList[clickData.Card.InstanceId])
        {
            Destroy(line.Item2.gameObject);
        }

        linesList.Remove(clickData.Card.InstanceId);
    }

    private static void startTransactionForNewAttacker(ClickData clickData)
    {
        var gameState = GameInfo.GameState;
        if (gameState.GetPlayer(gameState.CurrentPlayerId).BattleField.ContainsKey(clickData.Card.InstanceId))
        {
            if (gameState.GetPlayer(gameState.CurrentPlayerId).BattleField[clickData.Card.InstanceId].Type !=
                CardType.Unit)
                return;
            if (gameState.CurrentPhase == GamePhase.Attack)
                GameInfo.BackendController.ChooseAttackersTargets(clickData.Card.InstanceId);
            else if (gameState.CurrentPhase == GamePhase.Defence)
                GameInfo.BackendController.ChooseDefendersTargets(clickData.Card.InstanceId);
        }
    }

    public Dictionary<int, int> CollectAttackersAndClean()
    {
        var result = selectedAttackers;
        selectedAttackers = new Dictionary<int, int>();
        selectedTargetList = new Dictionary<int, TargetList>();
        return result;
    }

    public void cleanLines()
    {
        foreach (var lines in linesList)
        {
            foreach (var line in lines.Value)
            {
                try
                {
                    Destroy(line.Item2.gameObject);
                }
                catch (Exception e)
                {
                    //Unity sux
                }
            }
        }

        linesList = new Dictionary<int, List<Tuple<Target, LineBehaviour>>>();
    }
}