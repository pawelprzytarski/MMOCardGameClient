﻿using UnityEngine;

public interface IPlayedCardsZone
{
    GameObject AddPlayedCard(PlayedCardScriptable playedCardScriptable);
    void DestroyCard(int cardId);
}
