﻿using UnityEngine;

public interface ISimpleItemZoneController
{
	GameObject AddItemToScene(ScriptableObject itemToAdd, int id);
	void DestroyItem(int id);
}
