﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForegroundToReactionStackCard : MonoBehaviour
{
	[SerializeField] private Transform foreground;
	[SerializeField] private Transform cardSafeContainer;

	void Start ()
	{
		var zoneController = GetComponent<SimpleItemZoneController>();
		if (zoneController != null)
		{
			zoneController.AddedObjectMethod=AddedObjectMethod;
		}
	}

	private void AddedObjectMethod(GameObject gameobject)
	{
		var reactionStackCard = gameobject.GetComponent<ReactionStackCardDisplay>();
		if (reactionStackCard != null)
		{
			reactionStackCard.foreground = foreground;
			reactionStackCard.cardSafeContainer = cardSafeContainer;
		}
	}
}
