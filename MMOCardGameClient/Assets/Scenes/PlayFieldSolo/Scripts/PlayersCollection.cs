﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersCollection : MonoBehaviour {
    private List<PlayerInfos> players=new List<PlayerInfos>();
	
    public void AddPlayer(PlayerInfos player){
        players.Add(player);
    }

    public System.Collections.ObjectModel.ReadOnlyCollection<PlayerInfos> GetListOfPlayers(){
        return players.AsReadOnly();
    }

    public PlayerInfos GetPlayer(int id)
    {
        return players.Find(infos => infos.playerId==id);
    }
}
