﻿using System;
using Localization;
using UnityEngine;
using UnityEngine.UI;

public class FirstHandUserInfoTextDisplayer : MonoBehaviour
{
	public Text InfoText;

	void Start()
	{
		InfoText.text = string.Empty;
	}

	public void ShowInfoText(string localizedString)
	{
		InfoText.text = UserInfo.getLocalizedString(localizedString);
	}

	public void EmptyInfoText()
	{
		InfoText.text = string.Empty;
	}

}
