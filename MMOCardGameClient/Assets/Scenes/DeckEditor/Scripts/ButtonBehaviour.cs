﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DataClient;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonBehaviour : MonoBehaviour
{
	public NewDeckZoneController NewDeck;
	public LoadDeckFromServerScript DeckOnServer;
	public LoadingPanelBehaviour LoadingPanelBehaviour;
	public Button SaveButton;
	public Button CancelButton;

	private bool requestFinished;
	
	public void SaveDeck()
	{
		var cardsToAdd = new List<UserCardCollection>();
		var cardsToRemove = new List<UserCardCollection>();

		foreach (var cardCollection in NewDeck.CardsWithAmounts)
		{
			var amountInEditedDeck = cardCollection.Value;
			var amountToAdd = DeckOnServer.DeckOnServer.ContainsKey(cardCollection.Key)
				? amountInEditedDeck - DeckOnServer.DeckOnServer[cardCollection.Key]
				: amountInEditedDeck;

			if (amountToAdd > 0)
				cardsToAdd.Add(new UserCardCollection()
				{
					cardId = cardCollection.Key,
					amount = amountToAdd.ToString()
				});
			else if(amountToAdd < 0)
				cardsToRemove.Add(new UserCardCollection()
				{
					cardId = cardCollection.Key,
					amount = (-amountToAdd).ToString()
				});
		}

		StartCoroutine(saveDeckAsync(cardsToAdd, cardsToRemove));
	}

	private IEnumerator saveDeckAsync(IList<UserCardCollection> cardsToAdd, IList<UserCardCollection> cardsToRemove)
	{
		LoadingPanelBehaviour.disable = false;
		LoadingPanelBehaviour.InfoText.text = UserInfo.getLocalizedString("Saving deck...");
		disableButtons();
		
		requestFinished = false;
		if (cardsToAdd.Count > 0)
		{
			addCardsToServerDeck(cardsToAdd);
			yield return waitForRequestCompletion();
		}
		if (cardsToRemove.Count > 0)
		{
			removeCardsFromServerDeck(cardsToRemove);
			yield return waitForRequestCompletion();
		}
		
		SceneManager.LoadSceneAsync("DeckSelector");
	}

	private IEnumerator waitForRequestCompletion()
	{
		while (!requestFinished)
		{
			yield return new WaitForEndOfFrame();
		}

		requestFinished = false;
	}

	private void addCardsToServerDeck(IList<UserCardCollection> deckCardsList)
	{
		var request = UserInfo.LoggedClient.addCardsToDeck(UserInfo.DeckToEdit, deckCardsList);
		request.SuccessCallback = obj => { requestFinished = true; };
		request.ErrorCallback = obj =>
		{
			foreach (var error in obj)
			{
				Debug.LogError(error);
			}
		};
		request.Send();
	}

	private void removeCardsFromServerDeck(IList<UserCardCollection> deckCardsList)
	{
		var request = UserInfo.LoggedClient.deleteCardsFromDeck(UserInfo.DeckToEdit, deckCardsList);
		request.SuccessCallback = obj => { requestFinished = true; };
		request.ErrorCallback = obj =>
		{
			foreach (var error in obj)
			{
				Debug.LogError(error);
			}
		};
		request.Send();
	}

	public void CancelEditing()
	{
		SceneManager.LoadSceneAsync("DeckSelector");
	}

	private void disableButtons(bool value = true)
	{
		CancelButton.interactable = !value;
		SaveButton.interactable = !value;
	}
}
