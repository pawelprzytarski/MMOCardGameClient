﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionZoneController : MonoBehaviour
{
	public RectTransform CollectionZone;
	public GameObject CollectionCardPrefab;
	public EditorCollectionDisplay ZoneController;
	public NewDeckZoneController NewDeckZoneController;

	private bool allFieldsAreInitialized => CollectionZone != null && CollectionCardPrefab != null &&
	                                        ZoneController != null;

	void Start()
	{
		if(!allFieldsAreInitialized)
			Debug.LogError("NewDeckZoneController in \"" + gameObject.GetComponent<RectTransform>().name + 
			               "\" - not all fields are initialized!");
	}

	public void AddCard(int id, int amount = 1)
	{
		if (ZoneController.availableCards.ContainsKey(id))
			ZoneController.availableCards[id].GetComponent<IAmountChanger>().Increase(amount);
		else
			Debug.LogError("A wild card appeared! It's id is " + id);
	}

	public void RemoveCard(int id, int amount = 1)
	{
		if (!ZoneController.availableCards.ContainsKey(id)) return;
		var newAmount = ZoneController.availableCards[id].GetComponent<IAmountChanger>().Decrease(amount);
		if (newAmount < 0)
			Debug.LogWarning("No more cards with id " + id + " available! You just tried to pull a card out of thin air!");
		else
			NewDeckZoneController.AddCard(id, amount);
		
	}
}
