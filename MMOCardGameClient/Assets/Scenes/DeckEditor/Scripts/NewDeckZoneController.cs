﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewDeckZoneController : MonoBehaviour
{
	public RectTransform NewDeckZone;
	public GameObject MinimizedDeckCardPrefab;
	public CollectionZoneController CollectionZone;

	private bool allFieldsAreInitialized => NewDeckZone != null && MinimizedDeckCardPrefab != null;
	
	public Dictionary<int, GameObject> minimizedDeckCards { get; private set; }
	public Dictionary<int, int> CardsWithAmounts { get; private set; }
	
	void Start()
	{
		if(!allFieldsAreInitialized)
			Debug.LogError("NewDeckZoneController in \"" + gameObject.GetComponent<RectTransform>().name + 
			               "\" - not all fields are initialized!");
		minimizedDeckCards = new Dictionary<int, GameObject>();
		CardsWithAmounts = new Dictionary<int, int>();
	}

	public void AddCard(int id, int amount = 1)
	{
		if (minimizedDeckCards.ContainsKey(id))
			minimizedDeckCards[id].GetComponent<IAmountChanger>().Increase(amount);
		else
			addNewMinimizedCard(id, amount);

		if (CardsWithAmounts.ContainsKey(id))
			CardsWithAmounts[id] += amount;
		else
			CardsWithAmounts.Add(id, amount);
	}

	public void RemoveCard(int id, int amount = 1)
	{
		if (minimizedDeckCards.ContainsKey(id))
		{
			var newAmount = minimizedDeckCards[id].GetComponent<IAmountChanger>().Decrease(amount);
			if (newAmount <= 0)
			{
				Destroy(minimizedDeckCards[id].gameObject);
				minimizedDeckCards.Remove(id);
			}

			CollectionZone.AddCard(id, amount);
		}

		if (CardsWithAmounts.ContainsKey(id) && CardsWithAmounts[id] - amount >= 0)
			CardsWithAmounts[id] -= amount;

	}

	public List<UserCardCollection> GetCurrentDeck()
	{
		var list = new List<UserCardCollection>();
		foreach (var card in minimizedDeckCards)
		{
			var cardScriptable = (card.Value.GetComponent<IPlayfieldItemConfiguration>().Original as CardScriptable);
			if(cardScriptable == null) continue;
			list.Add(new UserCardCollection()
			{
				amount = cardScriptable.Amount.ToString(),
				cardId = card.Key
			});
		}

		return list;
	}

	private void addNewMinimizedCard(int id, int amount)
	{
		var instantiatedItem = Instantiate(MinimizedDeckCardPrefab);
		instantiatedItem.GetComponent<IPlayfieldItemConfiguration>()?.setUp(
			CardCreator.createNewCard(GameInfo.CardsCollection.getCard(id)));
		instantiatedItem.GetComponent<IAmountChanger>()?.Set(amount);
		instantiatedItem.GetComponent<RectTransform>().SetParent(NewDeckZone);
		minimizedDeckCards.Add(id, instantiatedItem);
	}
	
}
