using System;
using System.Collections.Generic;
using Backend.ClientCommunication;
using Backend.GameInformation;
using ProtoBuffers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using CardCosts = ProtoBuffers.CardCosts;
using Object = System.Object;

public class ClientCallbackScript : MonoBehaviour, IClientCallback
{
    delegate void Task();

    List<Task> tasks = new List<Task>();
    List<Task> rembemberedTasks = new List<Task>();
    Object tasksLock = new object();

    void Start()
    {
        DontDestroyOnLoad(this);
    }

    void Update()
    {
        List<Task> tasksToExecute;
        lock (tasksLock)
        {
            tasksToExecute = tasks;
            tasks = rembemberedTasks; 
            rembemberedTasks = new List<Task>();
        }

        foreach (var task in tasksToExecute)
        {
            try
            {
                task.Invoke();
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message+"\n\n\n"+e.StackTrace);
                CallbackListeners.get().OnClientSevereExceptionEvent("Message during executing CallbackTask: " + e.Message + "\n" + 
                                                       e.StackTrace, "ClientCallbackScript#Update", false);
            }
        }
    }
    
    void addTask(Task task, bool forFuture=false)
    {
        lock (tasksLock)
        {
            if(forFuture)
                rembemberedTasks.Add(task);
            else tasks.Add(task);
        }
    }

    public void ConnectionWasMade(GameState gameState)
    {
        addTask(() =>
        {
            SceneManager.LoadSceneAsync("SoloPlayfield");
            GameInfo.GameState = gameState;
        });
    }

    public void PhaseChanged(GamePhase oldPhase, int oldPlayerId, GamePhase newPhase, int newPlayerId)
    {
        addTask(() =>
        {
            Debug.Log("PhaseChanged: new phase: " + newPhase + " newPlayerId: " + newPlayerId + " old phase: " +
                      oldPhase +
                      " oldPlayerId: " + oldPlayerId);
            CallbackListeners.get().OnPhaseEndedEvent(oldPhase, oldPlayerId, newPhase, newPlayerId);
        });
    }

    public void PlayerDefenceChanged(int playerId, int newDefence)
    {
        addTask(() =>
        {
            Debug.Log("PlayerDefenceChanged: " + newDefence + " playerId: " + playerId);
            CallbackListeners.get().OnPlayerLifeChangedEvent(playerId, newDefence);
        });
    }

    public void LinkPointsUpdated(int playerId, LinkPoints playerLinkPoints)
    {
        addTask(() =>
        {
            Debug.Log("LinkPointsUpdated: " + playerLinkPoints + " playerId: " + playerId);
            CallbackListeners.get().OnLinkPointsUpdated(playerId, playerLinkPoints);
        });
    }


    public void HandChanged(int playerId)
    {
        addTask(() =>
        {
            Debug.Log("HandChanged for player with id " + playerId);
            CallbackListeners.get().OnHandChangeEvent(playerId);
        });
    }

    public void CardRevealed(Card revealedCard, int cardRevealedOwner, CardPosition cardRevealedPosition)
    {
        addTask(() =>
        {
            Debug.Log("CardRevealed: id:" + revealedCard.CardId + " instanceId: " + revealedCard.InstanceId);
            CallbackListeners.get().OnCardRevealed(revealedCard, cardRevealedOwner, cardRevealedPosition);
        }, true);
    }

    public void CardMoved(Card movedCard, int oldOwner, CardPosition oldPosition, int newOnwer,
        CardPosition newPosition)
    {
        addTask(() =>
        {
            Debug.Log("CardMoved: " + movedCard.CardId);
            CallbackListeners.get().OnCardMoved(movedCard, oldOwner, oldPosition, newOnwer, newPosition);
        });
    }

    public void PickOptions(int transactionId, TargetList targetList, int instanceId)
    {
        addTask(() =>
        {
            Debug.Log("PickOptions");
            CallbackListeners.get().OnPickOptions(transactionId, targetList, instanceId);
        });
    }

    public void YouFinishedChoosing(int transactionId, int instanceId)
    {
        addTask(() =>
        {
            Debug.Log("YouFinishedChoosing");
            CallbackListeners.get().OnFinishedChoosing(transactionId, instanceId);
        });
    }

    public void GotTargetListInResultOfTransaction(int transactionId, int instanceId, TargetList choosenTargets)
    {
        addTask(() =>
        {
            Debug.Log("YouFinishedChoosing");
            CallbackListeners.get().OnFinishedChoosingTargets(transactionId, instanceId, choosenTargets);
        });
    }

    public void YourElementAddedToStack(int stackId, StackElement stackElement)
    {
        addTask(() =>
        {
            Debug.Log("YourElementAddedToStack");
            CallbackListeners.get().OnElementAddedToStack(stackId, stackElement);
        });
    }

    public void OtherPlayerElementAddedToStack(int stackId, StackElement stackElement, int playerId,
        CardCosts cardCosts)
    {
        addTask(() =>
        {
            Debug.Log("OtherPlayerElementAddedToStack");
            CallbackListeners.get().OnElementAddedToStack(stackId, stackElement);
            //something more?
        });
    }

    public void ReactToStackAction()
    {
        addTask(() =>
        {
            Debug.Log("ReactToStackAction");
            CallbackListeners.get().OnReactToStackAction();
        });
    }

    public void WaitForReactionToStackAction(int playerId)
    {
        addTask(() =>
        {
            Debug.Log("WaitForReactionToStackAction");
            CallbackListeners.get().OnWaitForReactionToStackAction();
        });
    }

    public void StackActionCompleted(int stackId, StackElement stackElement)
    {
        addTask(() =>
        {
            Debug.Log("StackActionCompleted");
            CallbackListeners.get().OnStackActionCompleted(stackId, stackElement);
        });
    }

    public void StackActionRemoved(int stackId, StackElement stackElement)
    {
        addTask(() =>
        {
            Debug.Log("StackActionRemoved");
            CallbackListeners.get().OnStackActionRemoved(stackId, stackElement);
        });
    }

    public void AttackerDefenderSelected(Dictionary<int, List<TargetList>> attackersDefenders)
    {
        addTask(() =>
        {
            Debug.Log("AttackerDefenderSelected");
            CallbackListeners.get().OnAttackersSelected(attackersDefenders);
        });
    }

    public void AttackExecuted(int attackerId, int defenderId, TargetType defenderType)
    {
        addTask(() =>
        {
            Debug.Log("AttackExecuted");
            CallbackListeners.get().OnAttackExecuted(attackerId, defenderId, defenderType);
        });
    }

    public void CardStatisticsChanged(Card changedCard)
    {
        addTask(() =>
        {
            Debug.Log("CardStatisticsChanged");
            CallbackListeners.get().OnCardStatisticsChangedEvent(changedCard);
        });
    }

    public void YouWon()
    {
        addTask(() =>
        {
            CallbackListeners.get().OnGameEnded(GameEndedResult.Won);
            Debug.Log("YouWon");
        });
    }

    public void PlayerWon(int playerId)
    {
        CallbackListeners.get().OnPlayerWon(playerId);
        addTask(() => { Debug.Log("PlayerWon"); });
    }

    public void YouLost()
    {
        addTask(() =>
        {
            CallbackListeners.get().OnGameEnded(GameEndedResult.Lost);
            Debug.Log("YouLost");
        });
    }

    public void PlayerLost(int playerId)
    {
        addTask(() =>
        {
            CallbackListeners.get().OnPlayerLose(playerId);
            Debug.Log("PlayerLost");
        });
    }

    public void LibrarySizeChanged(int playerId)
    {
        addTask(() => { Debug.Log("LibrarySizeChanged"); });
    }

    public void HandSizeChanged(int playerId)
    {
        addTask(() => { Debug.Log("HandSizeChanged"); });
    }

    public void CardCreated(Card createdCard, int owner, CardPosition cardPosition)
    {
        addTask(() =>
        {
            CallbackListeners.get().OnCardCreated(createdCard, owner, cardPosition);
            Debug.Log("CardCreated");
        });
    }

    public void ServerError(Exception ex)
    {
        addTask(() => { Debug.LogError(ex.Message+"\n\n"+ex.StackTrace); });
    }

    public void TriggerAddedToStack(int stackId, StackElement stackElement)
    {
        addTask(() =>
        {
            Debug.Log("TriggerAddedToStack");
            CallbackListeners.get().OnTriggerAddedToStack(stackId, stackElement);
        });
    }

    public void TriggerWithoutStackActivated(int instanceId, TriggerType triggerType)
    {
        addTask(() =>
        {
            Debug.Log("TriggerActivatedWithoutStack");
            CallbackListeners.get().OnTriggerActivated(instanceId, triggerType);
        });
    }

    private void OnDestroy()
    {
        Debug.Log("Destroyed ClientCallback");
    }
}