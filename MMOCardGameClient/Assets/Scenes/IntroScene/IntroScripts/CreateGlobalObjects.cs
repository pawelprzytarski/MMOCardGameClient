﻿using Backend.Main;
using UnityEngine;

public class CreateGlobalObjects : MonoBehaviour
{
    public SceneManagment SceneManager;
    public BackendController BackendController;

    private void Start()
    {
        BackendController.BackendBuilder = new MainControllerBuilder();
        BackendController.BackendBuilder.SetCallbackObject(GetComponent<ClientCallbackScript>());
    }
}