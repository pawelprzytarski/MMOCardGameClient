﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsButtonBehaviour : MonoBehaviour 
{
	public GameObject SettingsPanel;

	void Start () {
        GetComponentInChildren<Text>().text =
            UserInfo.getLocalizedString("SettingsButton");
	}

	public void toggleVisibilityOfSettingsPanel()
	{
		bool active = SettingsPanel.activeInHierarchy;
		SettingsPanel.SetActive(!active);
	}
}
