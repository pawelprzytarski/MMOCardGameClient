﻿using UnityEngine;
using UnityEngine.UI;

public class AccountSettingsComponents : MonoBehaviour
{
    public ChangeEmailComponentsDataStructure ChangeEmailComponents;
    public Button EmailChangeButton;
    public Button PasswordChangeButton;

    public ChangePasswordScreenComponents PasswordScreenComponents;
}