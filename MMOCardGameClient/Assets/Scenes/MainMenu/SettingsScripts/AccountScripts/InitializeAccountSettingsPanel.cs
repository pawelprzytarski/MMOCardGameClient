﻿using UnityEngine;

public class InitializeAccountSettingsPanel : MonoBehaviour
{
    public AccountSettingsComponents AccountSettingsComponents;

    private void Start()
    {
    }

    private void Update()
    {
    }

    public void closeEmailChangingPanel()
    {
        AccountSettingsComponents.ChangeEmailComponents.Panel.SetActive(false);
        AccountSettingsComponents.EmailChangeButton.interactable = true;
        AccountSettingsComponents.PasswordChangeButton.interactable = false;
    }

    public void cloasePasswordChangignPanel()
    {
        AccountSettingsComponents.PasswordScreenComponents.Panel.SetActive(false);
        AccountSettingsComponents.PasswordChangeButton.interactable = true;
        AccountSettingsComponents.EmailChangeButton.interactable = false;
    }
}