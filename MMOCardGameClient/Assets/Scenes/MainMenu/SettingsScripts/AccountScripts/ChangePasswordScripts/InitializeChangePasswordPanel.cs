﻿using UnityEngine;

public class InitializeChangePasswordPanel : MonoBehaviour
{
    public ChangePasswordScreenComponents ChangePasswordComponents;

    private void Start()
    {
        ChangePasswordComponents.Panel.SetActive(false);
    }

    private void Update()
    {
    }

    public void displayChangePasswordPanel()
    {
        if (ChangePasswordComponents.Panel.activeInHierarchy)
        {
            ChangePasswordComponents.Panel.SetActive(false);
        }
        else
        {
            clearFormComponents();
            ChangePasswordComponents.Panel.SetActive(true);
        }
    }

    private void clearFormComponents()
    {
        ChangePasswordComponents.NewPasswordInputField.text = string.Empty;
        ChangePasswordComponents.RepeatNewPasswordInputField.text = string.Empty;
        ChangePasswordComponents.OldPasswordInputField.text = string.Empty;
    }
}