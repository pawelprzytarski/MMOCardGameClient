﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FormValidatorMonoBehaviour : MonoBehaviour
{
    public InputField EmailInputField;
    public InputField RepeatedEmailInputField;
    public InputField LoginInputField;
    public InputField OldPasswordInputField;
    public InputField PasswordInputField;
    public InputField RepeatedPasswordInputField;


    public Text EmailInfoText;
    public Text RepeatedEmailInfoText;
    public Text LoginInfoText;
    public Text OldPasswordInfoText;
    public Text PasswordInfoText;
    public Text RepeatedPasswordInfoText;

    private FormDataValidator validator;
	
	void Start ()
	{
	    validator = new FormDataValidator();
	}

    public void validateEmail()
    {
        if(EmailInputField == null || EmailInfoText == null) return;
        if (validator.validateEmail(EmailInputField))
            setTextAsInfo(EmailInfoText, UserInfo.getLocalizedString("ValidInformation"), Color.white);
        else
            setTextAsInfo(EmailInfoText, UserInfo.getLocalizedString("InvalidEmail"), Color.red);
    }

    public void validateIfEmailsAreTheSame()
    {
        if(EmailInputField == null || RepeatedEmailInputField == null) return;
        if(validator.checkIfFieldsAreEqual(EmailInputField, RepeatedEmailInputField))
            setTextAsInfo(RepeatedEmailInfoText, UserInfo.getLocalizedString("ValidInformation"), Color.white);
        else
            setTextAsInfo(RepeatedEmailInfoText, UserInfo.getLocalizedString("EmailsDontMatch"), Color.red);
    }

    public void validatePassword()
    {
        if (PasswordInputField == null || PasswordInfoText == null) return;
        if(validator.validatePassword(PasswordInputField))
            setTextAsInfo(PasswordInfoText, UserInfo.getLocalizedString("ValidInformation"), Color.white);
        else
            setTextAsInfo(PasswordInfoText, UserInfo.getLocalizedString("InvalidPassword"), Color.red);
    }

    public void validatateIfPasswordsMatch()
    {
        if(PasswordInputField == null || RepeatedPasswordInputField == null || RepeatedPasswordInfoText == null) return;
        if(validator.checkIfFieldsAreEqual(PasswordInputField, RepeatedPasswordInputField))
            setTextAsInfo(RepeatedPasswordInfoText, UserInfo.getLocalizedString("ValidInformation"), Color.white);
        else
            setTextAsInfo(RepeatedPasswordInfoText, UserInfo.getLocalizedString("PasswordsDontMatch"), Color.red);
    }

    public void validateOldPassword()
    {
        if(OldPasswordInputField == null || OldPasswordInfoText == null) return;
        if(validator.validatePassword(OldPasswordInputField))
            setTextAsInfo(OldPasswordInfoText, UserInfo.getLocalizedString("ValidInformation"), Color.white);
        else
            setTextAsInfo(OldPasswordInfoText, UserInfo.getLocalizedString("InvalidPassword"), Color.red);
    }

    public void validateLogin()
    {
        if(LoginInputField == null || LoginInfoText == null) return;
        if(validator.validateLogin(LoginInputField))
            setTextAsInfo(LoginInfoText, UserInfo.getLocalizedString("ValidInformation"), Color.white);
        else
            setTextAsInfo(LoginInfoText, UserInfo.getLocalizedString("InvalidLogin"), Color.red);
    }

    private void setTextAsInfo(Text infoText, string infoString, Color color)
    {
        infoText.text = infoString;
        infoText.color = color;
    }
}
