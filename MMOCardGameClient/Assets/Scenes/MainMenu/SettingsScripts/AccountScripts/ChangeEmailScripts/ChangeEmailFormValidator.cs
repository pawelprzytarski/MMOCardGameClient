﻿public class ChangeEmailFormValidator : FormDataValidator
{
    private readonly ChangeEmailComponentsDataStructure Data;

    public ChangeEmailFormValidator(ChangeEmailComponentsDataStructure dataStructure)
    {
        Data = dataStructure;
    }

    public bool validateForm()
    {
        return validateEmail(Data.NewEmailInputField) &&
               validateEmail(Data.RepeatNewEmailInputField) &&
               checkIfFieldsAreEqual(Data.NewEmailInputField, Data.RepeatNewEmailInputField) &&
               validatePassword(Data.PasswordInputField);
    }
}