﻿using UnityEngine;

public class InitualizeChangeEmailPanel : MonoBehaviour
{
    public ChangeEmailComponentsDataStructure ChangeEmailComponents;

    private void Start()
    {
        ChangeEmailComponents.Panel.SetActive(false);
    }

    private void Update()
    {
    }

    public void displayChangeEmailPanel()
    {
        if (ChangeEmailComponents.Panel.activeInHierarchy)
        {
            ChangeEmailComponents.Panel.SetActive(false);
        }
        else
        {
            clearFormComponents();
            ChangeEmailComponents.Panel.SetActive(true);
        }
    }

    private void clearFormComponents()
    {
        ChangeEmailComponents.NewEmailInputField.text = string.Empty;
        ChangeEmailComponents.RepeatNewEmailInputField.text = string.Empty;
        ChangeEmailComponents.PasswordInputField.text = string.Empty;
    }
}