﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeEmailComponentsDataStructure : MonoBehaviour
{
    public Button ChangeEmailButton;

    public Text NewEmailInfoText;

    public InputField NewEmailInputField;

    public GameObject Panel;
    public Text PasswordInfoText;
    public InputField PasswordInputField;
    public Text RepeatNewEmailInfoText;
    public InputField RepeatNewEmailInputField;
    public Text TitleText;
    public Text ServerInfoText;
}