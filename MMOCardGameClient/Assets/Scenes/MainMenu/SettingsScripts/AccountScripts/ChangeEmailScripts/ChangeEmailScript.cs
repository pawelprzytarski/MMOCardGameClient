﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class ChangeEmailScript : MonoBehaviour
{
    private bool buttonsLocked;
    public ChangeEmailComponentsDataStructure ChangeEmailComponents;
    private bool emailChanged;

    private ChangeEmailFormValidator FormValidator;


    private void Start()
    {
        buttonsLocked = false;
        emailChanged = false;
        FormValidator = new ChangeEmailFormValidator(ChangeEmailComponents);
    }

    private void Update()
    {
        if (ChangeEmailComponents.Panel.activeInHierarchy)
            enableChangeEmailButtonIfPossible();
    }

    private void enableChangeEmailButtonIfPossible()
    {
        if (!buttonsLocked && !emailChanged)
            ChangeEmailComponents.ChangeEmailButton.interactable = formDataIsValid();
        else
            ChangeEmailComponents.ChangeEmailButton.interactable = false;
    }

    private bool formDataIsValid()
    {
        return FormValidator.validateForm() &&
               ChangeEmailComponents.RepeatNewEmailInputField.text.Equals(ChangeEmailComponents.NewEmailInputField
                   .text);
    }

    public void changeEmail()
    {
        disableButtons();
        if (formDataIsValid())
            StartCoroutine(changeEmailAsync());
    }

    private void disableButtons()
    {
        ChangeEmailComponents.ChangeEmailButton.interactable = false;

        ChangeEmailComponents.NewEmailInputField.interactable = false;
        ChangeEmailComponents.RepeatNewEmailInputField.interactable = false;
        ChangeEmailComponents.PasswordInputField.interactable = false;
        buttonsLocked = true;
    }

    private IEnumerator changeEmailAsync()
    {
        var changeEmailRequestBuilder = new ChangeEmailRequestBuilder(getFormData(), UnityWebRequest.kHttpVerbPUT);
        yield return changeEmailRequestBuilder.WebRequest.SendWebRequest();
               
        getResponse(changeEmailRequestBuilder.WebRequest).getMessageForUser(ref ChangeEmailComponents.ServerInfoText);
        enableButtons();
    }

    private DataServerResponse getResponse(UnityWebRequest webRequest)
    {
        return new DataServerResponse.DataServerResponseBuilder(webRequest, "Change Email")
            .addResponseCodeMessage(HttpCode.Ok, UserInfo.getLocalizedString("EmailChanged"), true)
            .addResponseCodeMessage(HttpCode.BadRequest, UserInfo.getLocalizedString("EmailChangingError"), false)
            .addResponseCodeMessage(HttpCode.NotFound, UserInfo.getLocalizedString("EmailChangingError"), false)
            .addResponseCodeMessage(HttpCode.Forbidden, UserInfo.getLocalizedString("UserDoesNotExist"), false)
            .build();
    }

    private ChangeEmailFormBody getFormData()
    {
        return new ChangeEmailFormBody()
        {
            newEmail = ChangeEmailComponents.NewEmailInputField.text,
            repeatedEmail = ChangeEmailComponents.RepeatNewEmailInputField.text,
            password = ChangeEmailComponents.PasswordInputField.text
        };
    }

    private void enableButtons()
    {
        ChangeEmailComponents.ChangeEmailButton.interactable = true;

        ChangeEmailComponents.NewEmailInputField.interactable = true;
        ChangeEmailComponents.RepeatNewEmailInputField.interactable = true;
        ChangeEmailComponents.PasswordInputField.interactable = true;
        buttonsLocked = false;
    }
}