﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayButtonBehaviour : MonoBehaviour
{
    public void changeScene()
    {
        SceneManager.LoadSceneAsync("GameMenu");
    }
}
