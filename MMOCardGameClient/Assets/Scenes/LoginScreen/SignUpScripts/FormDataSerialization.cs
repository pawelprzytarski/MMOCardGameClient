﻿using Newtonsoft.Json;

public class FormDataSerialization
{
    private readonly FormDataBody Body;

    public FormDataSerialization(FormDataBody body)
    {
        Body = body;
    }

    public FormDataSerialization(string email, string login, string password,
        string repeatedPassword)
    {
        Body = new FormDataBody
        {
            email = email,
            login = login,
            password = password,
            repeatedPassword = repeatedPassword
        };
    }

    public string serializeFormDataBody()
    {
        return JsonConvert.SerializeObject(Body);
    }
}

public class FormDataBody
{
    public string email;
    public string login;
    public string password;
    public string repeatedPassword;
}