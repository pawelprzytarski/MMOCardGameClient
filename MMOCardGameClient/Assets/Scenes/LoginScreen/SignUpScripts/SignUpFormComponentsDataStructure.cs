﻿using UnityEngine;
using UnityEngine.UI;

public class SignUpFormComponentsDataStructure : MonoBehaviour
{
    public Button CancelButton;

    public Text EmailInfoText;
    public InputField EmailInputField;

    public Canvas LoadingCircleCanvas;

    public Button MessageOkbutton;
    public Text PasswordInfoText;
    public InputField PasswordInputField;
    public Text RepeatedPasswordInfoText;
    public InputField RepeatPasswordInputfield;
    public Text ServerResponseText;
    public Button SignUpButton;
    public GameObject SignUpFormPanel;

    public Text SignUpSuccessMessageTitle;

    public GameObject SuccessMessagePanel;

    public Text TitleText;
    public Text UserNameInfoText;
    public InputField UserNameInputfield;
}