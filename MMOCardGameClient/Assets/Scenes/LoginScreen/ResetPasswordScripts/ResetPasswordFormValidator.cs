﻿public class ResetPasswordFormValidator : FormDataValidator
{
    private readonly ResetPasswordComponentsDataStructure Data;

    public ResetPasswordFormValidator(ResetPasswordComponentsDataStructure dataStructure)
    {
        Data = dataStructure;
    }

    public bool validateResetFormWithoutLogin()
    {
        return validatePassword(Data.NewPasswordInputField) &&
               validatePassword(Data.RepeatNewPasswordInputField) &&
               checkIfFieldsAreEqual(Data.NewPasswordInputField, Data.RepeatNewPasswordInputField);
    }

    public bool validateLogin()
    {
        return base.validateLogin(Data.LoginInputField);
    }

    public bool validateNewPassword()
    {
        return validatePassword(Data.NewPasswordInputField);
    }

    public bool validateRepeatedPassword()
    {
        return validatePassword(Data.RepeatNewPasswordInputField);
    }
}