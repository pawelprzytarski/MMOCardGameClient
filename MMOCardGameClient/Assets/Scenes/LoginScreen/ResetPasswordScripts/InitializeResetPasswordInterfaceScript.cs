﻿using UnityEngine;
using UnityEngine.UI;

public class InitializeResetPasswordInterfaceScript : MonoBehaviour
{
    public ResetPasswordComponentsDataStructure ResetPasswordComponents;

    private void Start()
    {
        initializeResetPasswordPanel();
    }

    private void initializeResetPasswordPanel()
    {
        ResetPasswordComponents.ResetTokenInfoText.text = string.Empty;
        ResetPasswordComponents.NewPasswordInfoText.text = string.Empty;
        ResetPasswordComponents.RepeatNewPasswordInfoText.text = string.Empty;
        ResetPasswordComponents.LoginInfoText.text = string.Empty;

        ResetPasswordComponents.ResetPasswordButton.interactable = false;
        ResetPasswordComponents.ResetPasswordWindowPanel.SetActive(false);
        ResetPasswordComponents.LoadingCircle.SetActive(false);

        ResetPasswordComponents.ResetTokenInputField.interactable = false;
        ResetPasswordComponents.NewPasswordInputField.interactable = false;
        ResetPasswordComponents.RepeatNewPasswordInputField.interactable = false;
    }
}