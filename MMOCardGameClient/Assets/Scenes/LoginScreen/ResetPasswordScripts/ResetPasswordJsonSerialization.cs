﻿using Newtonsoft.Json;

public class ResetPasswordJsonSerialization
{
    private ResetJsonBody resetBody;

    public ResetPasswordJsonSerialization(string token, string newPassword, string repeatedNewPassword)
    {
        resetBody = new ResetJsonBody()
        {
            newPassword = newPassword,
            repeatedNewPassword = repeatedNewPassword,
            token = token
        };
    }

    public ResetPasswordJsonSerialization(ResetJsonBody resetJsonBody)
    {
        resetBody = resetJsonBody;
    }

    public string serialize()
    {
        return JsonConvert.SerializeObject(resetBody);
    }
}

public class ResetJsonBody
{
    public string newPassword;
    public string repeatedNewPassword;
    public string token;
}