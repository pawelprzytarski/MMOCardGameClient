﻿using System.Text.RegularExpressions;

public static class LoginCredentialsValidator
{
    private static readonly Regex loginValidationRegex = new Regex(@"^[A-Za-z0-9]{3,20}$");
    private static readonly Regex passwordValidationRegex = new Regex(@"^(?=\S+$).[A-Za-z0-9@#$%^&+=]{7,35}$");

    private static bool loginValid;
    private static bool passwordValid;


    public static bool validateLoginData(string userName, string password)
    {
        loginValid = isLoginValid(userName);
        passwordValid = isPasswordValid(password);

        return loginValid && passwordValid;
    }

    public static bool validateLoginData()
    {
        return validateLoginData(
            UserInfo.UserName,
            UserInfo.Password
        );
    }

    private static bool isLoginValid(string value)
    {
        return loginValidationRegex.IsMatch(value);
    }

    private static bool isPasswordValid(string value)
    {
        return passwordValidationRegex.IsMatch(value);
    }

    public static string getValidationError()
    {
        if (!loginValid && !passwordValid)
            return UserInfo.getLocalizedString(
                "InvalidLoginAndPassword");
        if (!loginValid)
            return UserInfo.getLocalizedString(
                "InvalidLogin"); 
        if (!passwordValid) 
            return UserInfo.getLocalizedString("InvalidPassword");
        else return string.Empty;
    }
}