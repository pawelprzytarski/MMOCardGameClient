﻿using UnityEngine;
using UnityEngine.UI;

public class MenuButtonControlScript : MonoBehaviour
{
    public Button ExitButton;
    public Button LoginButton;

    public InputField loginInputField;
    public InputField passwordInputfield;
    public Button RegisterButton;


    private void Start()
    {
        if (loginInputField != null && passwordInputfield != null) checkIfLoginButtonCanBeEnabled();
    }

    public void checkIfLoginButtonCanBeEnabled()
    {
        if (loginInputField.text.Length == 0 || passwordInputfield.text.Length == 0)
            disableLoginButton();
        else
            enableLoginButton();
    }

    public void enableLoginButton()
    {
        LoginButton.interactable = true;
    }

    public void disableLoginButton()
    {
        LoginButton.interactable = false;
    }

    public void disableRegisterButton()
    {
        RegisterButton.interactable = false;
    }

    public void enableRegisterButton()
    {
        RegisterButton.interactable = true;
    }

    public void disableQuitButton()
    {
        ExitButton.interactable = false;
    }

    public void enableQuitButton()
    {
        ExitButton.interactable = true;
    }
}