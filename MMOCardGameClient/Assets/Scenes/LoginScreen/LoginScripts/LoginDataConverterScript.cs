﻿using System;
using System.Text;

public class LoginDataConverterScript
{
    public const string BASE64_PREAMBLE = "Basic";

    private readonly string loginPlaintext;
    private readonly string passwordPlaintext;
    private string base64LoginString;
    private string plaintextLoginString;

    public LoginDataConverterScript(string loginPlaintext, string passwordPlaintext)
    {
        this.loginPlaintext = loginPlaintext;
        this.passwordPlaintext = passwordPlaintext;
    }

    public string convertLoginDataToBase64LoginString()
    {
        plaintextLoginString = buildPlaintextLoginString();
        base64LoginString = convertPlaintextLoginStringToBASE64();

        return buildBase64LoginString();
    }

    private string buildPlaintextLoginString()
    {
        var plaintextLoginString = new StringBuilder();
        plaintextLoginString.Append(loginPlaintext);
        plaintextLoginString.Append(":");
        plaintextLoginString.Append(passwordPlaintext);
        return plaintextLoginString.ToString();
    }

    private string convertPlaintextLoginStringToBASE64()
    {
        var plainTextBytes = Encoding.UTF8.GetBytes(plaintextLoginString);
        return Convert.ToBase64String(plainTextBytes);
    }

    private string buildBase64LoginString()
    {
        var base64RequestStringBuilder = new StringBuilder();
        base64RequestStringBuilder.Append(BASE64_PREAMBLE);
        base64RequestStringBuilder.Append(" ");
        base64RequestStringBuilder.Append(base64LoginString);

        return base64RequestStringBuilder.ToString();
    }
}