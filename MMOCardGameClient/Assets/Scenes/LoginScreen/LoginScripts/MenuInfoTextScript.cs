﻿using UnityEngine;
using UnityEngine.UI;

public class MenuInfoTextScript : MonoBehaviour
{
    public Text menuInfoText;

    // Use this for initialization
    private void Start()
    {
    }

    public void setText(string text, Color color)
    {
        setText(text);
        setColor(color);
    }

    public void setText(string text)
    {
        menuInfoText.text = text;
    }

    public void setColor(Color color)
    {
        menuInfoText.color = color;
    }
}