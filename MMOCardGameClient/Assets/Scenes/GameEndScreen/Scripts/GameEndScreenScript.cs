﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using ExtensionMethods;
using System;
using System.Collections;
using DataClient;
using UnityEngine.Networking;

public class GameEndScreenScript : MonoBehaviour
{
    public Text resultText;
    public Text awardText;
    public GameObject resultPanel;

    void Start()
    {
        resultText.text = "Unknown result";
        request();
    }

    private void request()
    {
        var request = UserInfo.LoggedClient.getLastGameResult();

        request.SuccessCallback += setGameEndData;
        request.ErrorCallback += obj =>
        {
            foreach (var error in obj)
            {
                Debug.LogError(error);                
            }
            resultText.text = "Error";
        };
        
        request.Send();
    }

    private void setGameEndData(GameEndData gameEndData)
    {
        resultText.text = UserInfo.getLocalizedString(gameEndData.result);
        awardText.text = gameEndData.award.ToString();
    }

    public void CloseScreen()
    {
        SceneManager.UnloadSceneAsync("GameEndScreen");
        SceneManager.LoadSceneAsync("MenuScreen");
    }
}