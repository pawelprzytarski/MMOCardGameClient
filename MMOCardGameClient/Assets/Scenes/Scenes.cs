﻿using System;

public enum Scenes
{
    CURRENT,
    LOGIN_SCREEN,
    MAIN_MENU,
    CARD_COLLECTION,
    INTRO_SCREEN,
    GAME_MENU,
    PLAY_FIELD
}

public static class SceneNames
{
    public static string getSceneName(Scenes scene)
    {
        switch (scene)
        {
            case Scenes.LOGIN_SCREEN:
                return @"LoginScreen";
            case Scenes.MAIN_MENU:
                return @"MenuScreen";
            case Scenes.INTRO_SCREEN:
                return @"Intro";
            case Scenes.CARD_COLLECTION:
                return @"CardCollection";
            case Scenes.GAME_MENU:
                return @"GameMenu";
            case Scenes.PLAY_FIELD:
                return @"SoloPlayfield";
            default:
                throw new Exception("No such screen " + scene.ToString());
        }
    }
}