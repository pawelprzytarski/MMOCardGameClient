﻿using System.Collections;
using System.Collections.Generic;
using Backend.GameInformation;
using Backend.Main;

public static class GameInfo
{
    private static Configuration configuration = null;

    public static Configuration Configuration
    {
        set { configuration = value; }
    }

    public static IMainController BackendController { get; set; }
    
    public static GameState GameState { get; set; }

    public static string Token { get; set; }
    
    public static int PlayerId { get; set; }

    public static string ServerAdress1 => configuration.battleServerIpAdress;

    public static NamedAbilities NamedAbilities { get; set; } = new NamedAbilities();
    
    public static CardsCollection CardsCollection { get; set; } = new CardsCollection();
}