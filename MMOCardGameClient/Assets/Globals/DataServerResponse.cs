﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;

public class DataServerResponse : DataServerResponseBase
{
    public DataServerResponse(UnityWebRequest webRequest, string requestName) : 
        base(webRequest, requestName)
    {
        Success = checkForNetworkError();
    }

    public class DataServerResponseBuilder
    {
        private readonly DataServerResponse _response;

        private bool responseCodeHandled = false;

        public DataServerResponseBuilder(UnityWebRequest webRequest, string requestName)
        {
            _response = new DataServerResponse(webRequest, requestName);
        }

        public DataServerResponseBuilder addResponseCodeMessage(params ResponseMessage[] messages)
        {
            foreach (var responseMessage in messages) setMessage(responseMessage);
            return this;
        }

        public DataServerResponseBuilder addResponseCodeMessage(ResponseMessage message)
        {
            setMessage(message);
            return this;
        }

        public DataServerResponseBuilder addResponseCodeMessage(HttpCode responseCode, string message, bool success)
        {
            setMessage(new ResponseMessage() {Message = message, ResponseCode = (long)responseCode,
                Success = success});
            return this;
        }

        public DataServerResponseBuilder setDefaultMessage(string message)
        {
            _response.defaultMessage = message;
            return this;
        }

        private DataServerResponseBuilder setMessage(ResponseMessage responseMessage)
        {
            if (responseMessage.ResponseCode != _response.ResponseCode) return this;
            if (!_response.isNetworkError) _response.message = responseMessage.Message;
            _response.Success = _response.Success && responseMessage.Success;
            responseCodeHandled = true;
            return this;
        }


        public DataServerResponse build()
        {
            if (operationSuccessProvenByResponseCodeOrEmptyMessage()) return _response;
            _response.message = _response.defaultMessage;
            Debug.Log(_response.getDebugErrorString("Uknown error"));

            return _response;
        }

        private bool operationSuccessProvenByResponseCodeOrEmptyMessage()
        {
            return _response.Success && responseCodeHandled ||
                   !string.IsNullOrEmpty(_response.message);
        }

    }
}



public struct ResponseMessage
{
    public long ResponseCode;
    public string Message;
    public bool Success;
}
