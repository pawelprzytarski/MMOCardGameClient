﻿using System;
using System.Text;
using Backend.ClientCommunication;
using Backend.Constants;
using Backend.Main;
using UnityEngine;

public class MainControllerBuilder : IBackendBuilder
{
	private MainController mainController;

	private string userName;
	private IClientCallback clientCallbackObject;
	private readonly string pathToCardsJSON;
	private readonly string pathToLogFile;

	private bool userNameChanged;
	private bool callbackChanged;

	public bool ChangesOccurred()
	{
		return mainController == null || callbackChanged || userNameChanged;
	}

	public MainControllerBuilder()
	{
		mainController = null;
		userName = string.Empty;
		clientCallbackObject = null;
		pathToCardsJSON = StaticPaths.CardsJsonDirectory;
		pathToLogFile = StaticPaths.ErrorLogLocation;
		setChangeInfoToNoChanges();
	}

	private void setChangeInfoToNoChanges()
	{
		callbackChanged = false;
		userNameChanged = false;
	}
	

	public IBackendBuilder SetUserName(string userName)
	{
		if (this.userName.Equals(userName)) return this;
		this.userName = userName;
		userNameChanged = true;
		return this;
	}

	public IBackendBuilder SetCallbackObject(IClientCallback callback)
	{
		clientCallbackObject = callback;
		callbackChanged = true;
		return this;
	}
	

	public IMainController Build()
	{
		//if(!ChangesOccurred()) return mainController;
		checkParameters();
		mainController = new MainController(userName, clientCallbackObject, pathToCardsJSON, pathToLogFile);
		setChangeInfoToNoChanges();
		return mainController;
	}

	private void checkParameters()
	{
		var errorMessage = new StringBuilder();
		errorMessage.Append("MainControllerBuilder: ");
		var initialLength = errorMessage.Length;
		if (string.IsNullOrEmpty(userName)) errorMessage.Append("\nUsername can't be empty. ");
		if (clientCallbackObject == null) errorMessage.Append("\nClient callback isn't set.");
		if(errorMessage.Length > initialLength) throw new Exception(errorMessage.ToString());
	}
}
