﻿using UnityEngine;

public class CardHandSizeCalculator : ItemSizeCalculatorBase
{
	private readonly float heightToWidth;
	private readonly float widthToHeight;

	public CardHandSizeCalculator(RectTransform cardZone, Margin margin, float spacingPercent, int itemCount,
		Vector2 aspectRatio)
		: base(new Vector2(cardZone.rect.width, cardZone.rect.height), margin, spacingPercent, itemCount, aspectRatio)
	{
		heightToWidth = aspectRatio.x / aspectRatio.y;
		widthToHeight = aspectRatio.y / aspectRatio.x;
	}

	protected override void setItemSize()
	{
		calculateUsableZoneSize();
		calculateCardSize();
	}

	private void calculateCardSize()
	{
		var percentOfSpaceUsedByCards = (1 + (itemCount - 1) * spacing) / 100;
		var itemHeightKeepingAspectRatio = usableZoneSize.y;
		var itemWidthKeepingAspectRatio = (usableZoneSize.x / percentOfSpaceUsedByCards);
		var necessaryHeightUsingMaxWidth = itemWidthKeepingAspectRatio * widthToHeight;
		
		itemSize = necessaryHeightUsingMaxWidth > usableZoneSize.y ? 
			new Vector2(itemHeightKeepingAspectRatio * heightToWidth, itemHeightKeepingAspectRatio) : 
			new Vector2(itemWidthKeepingAspectRatio, itemWidthKeepingAspectRatio * widthToHeight);
	}	
}
