﻿using UnityEngine;

public class SimpleItemSizeCalculator : ItemSizeCalculatorBase
{
    private readonly bool keepAspectRatio;
    
    public SimpleItemSizeCalculator(Vector2 zoneSize, Margin margin, float spacing, int itemCount, Vector2 aspectRatio,
        bool keepAspectRatio) : base(zoneSize, margin, spacing, itemCount, aspectRatio)
    {
        this.keepAspectRatio = keepAspectRatio;
    }

    protected override void setItemSize()
    {
        calculateUsableZoneSize();
        if(keepAspectRatio) 
            calculateItemSizeKeepingAspectRatio();
        else  
            calculateBiggestPossibleItemSize();
    }
    
    private void calculateItemSizeKeepingAspectRatio()
    {
        var itemHeightKeepingAspectRatio = usableZoneSize.y;
        var	itemWidthKeepingAspectRatio = usableZoneSize.y * aspectRatio.x / aspectRatio.y;
        var necessarySpace = (itemWidthKeepingAspectRatio + spacing) * itemCount - spacing;
        if (necessarySpace >= usableZoneSize.x)
        {
            var sizeDivider = (necessarySpace / usableZoneSize.x);
            itemWidthKeepingAspectRatio /= sizeDivider;
            itemHeightKeepingAspectRatio /= sizeDivider;
        }
			
        ItemSize = new Vector2(itemWidthKeepingAspectRatio, itemHeightKeepingAspectRatio);	
    }

    private void calculateBiggestPossibleItemSize()
    {
        var itemWidth = (usableZoneSize.x - spacing * (itemCount - 1)) / itemCount; 
        ItemSize = new Vector2(itemWidth, usableZoneSize.y);
    }
}
