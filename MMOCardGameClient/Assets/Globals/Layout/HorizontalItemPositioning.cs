﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class HorizontalItemPositioning : Positioning
{
	private HorizontalAlignment _alignment;
	private float spacing;
	private Vector2 itemSize;

	private Vector2 aspectRatio;
	private bool keepAspectRatio;
	
	public HorizontalItemPositioning(RectTransform zone, Margin margin, 
		Vector2 aspectRatio, HorizontalAlignment alignment, float spacing, bool keepAspectRatio)
		: base(zone, margin)
	{
		this.spacing = spacing;
		this._alignment = alignment;
		this.aspectRatio = aspectRatio;
		this.keepAspectRatio = keepAspectRatio;

		checkAndCorrectAspectRatio();
	}

	private void checkAndCorrectAspectRatio()
	{
		if(aspectRatio.x == 0 || aspectRatio.y == 0)
			aspectRatio = Vector2.one;
	}

	
	public void setPositions()
	{
		itemSize = 
			new SimpleItemSizeCalculator(zoneSize, margin, spacing, itemZone.childCount, aspectRatio, keepAspectRatio).ItemSize;
		var beginXCoordinate = getBeginningXCoordinate();
		for (var i = 0; i < itemZone.childCount; i++)
		{
			itemZone.GetChild(i).GetComponent<RectTransform>().sizeDelta = itemSize;
			
			var newPosition = getItemPosition(beginXCoordinate, i, 0);
            
			var child=itemZone.GetChild(i);
			var movableChild = child.GetComponent<IMovable>();
			if (movableChild != null)
			{
				movableChild.moveToPosition(newPosition.x, newPosition.y);
				movableChild.resizeToSize(itemSize.x, itemSize.y);
			}
			else
			{
				child.transform.position = newPosition;
				child.GetComponent<RectTransform>().sizeDelta = itemSize;
			}
		}	
	}

	private float getBeginningXCoordinate()
	{
		switch (_alignment)
		{
			case HorizontalAlignment.CENTER:
				return ((itemSize.x + spacing) * itemZone.childCount - spacing) / -2 + margin.Left / 2 - margin.Right / 2 + itemSize.x / 2;
			case HorizontalAlignment.LEFT:
				return (zoneSize.x / -2) + margin.Left + itemSize.x / 2;
			case HorizontalAlignment.RIGHT:
				return (zoneSize.x / 2 - ((itemSize.x + spacing) * itemZone.childCount - spacing)) - margin.Right + itemSize.x / 2;
			default:
				throw new Exception("Unsupported orientation. Also, this should be unreachable");
		}
	}
	
	private Vector3 getItemPosition(float beginXCoordinate, int i, float yCoordinate)
	{
		var x = beginXCoordinate + (itemSize.x + spacing) * i;
		var position = itemZone.position + 
		               new Vector3(x, yCoordinate, 0);
		return position;
	}
}
