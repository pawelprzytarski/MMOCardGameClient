﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AllCardsCollection
{
	private static IAllCardsCollection _collection;

	public static CardScriptable GetCardById(int id)
	{
		return _collection.GetCardById(id);
	}
}
