﻿using System.Globalization;
using DataClient;
using JSON.Readers.CardReader;
using Localization;
using UnityEngine;

public static class UserInfo
{
    private static Configuration configuration = null;

    public static LocalizationDictionary LocalizationDictionary { get; set; }

    public static string getLocalizedString(string str)
    {
        if (LocalizationDictionary == null)
            LocalizationDictionary = new LocalizationLoader().loadLocalizationFor(CultureInfo.CurrentCulture.Name);
        return LocalizationDictionary.getLocalizedTextIfExists(str);
    }

    public static Configuration Configuration
    {
        set { configuration = value; }
    }

    public static string Language => configuration.language;

    public static string UserName { get; set; }

    public static string Password { get; set; }

    public static string AuthenticationToken { get; set; }

    
    public static string DataServerUrlAuthenticated => configuration.dataServerURL + configuration.authentionUrlTail;

    public static string DataServerUrlUsers => configuration.dataServerURL + configuration.usersURLTail;

    public static string DataServerURL => configuration.dataServerURL;

    public static string UsersUrlTail => configuration.usersURLTail;

    public static string EmailUrlTail => configuration.changeEmailTail;

    public static string ChangePasswordTail => configuration.changePasswordTail;

    public static string ResetPasswordUrlTail => configuration.resetPasswordTail;

    public static string CardsCollectionTail => configuration.cardsCollectionTail;

    public static string GameUrlTail => configuration.gameTail;

    public static string GameResultUrlTail => configuration.gameResultUrlTail;

    public static string CheckIfUserIsInGameTail => configuration.checkIfUserIsInGameTail;

    public static string SoloDoubleDuelUrlTail => configuration.soloDoubleDuel;

    public static string SoloThreeDuelUrlTail => configuration.soloThreeDuel;

    public static string SoloFourDuelUrlTail => configuration.soloFourDuel;

    public static string TeamDoubleDuelUrlTail => configuration.teamDoubleDuel;

    public static int BattleServerConnectionRetries => configuration.nrOfBattleServerConnectionRetries;

    public static string DeckToEdit { get; set; } = string.Empty;

    public static Color ErrorColor = Color.red;


    public static LoginStatus loginStatus { get; set; } = LoginStatus.SIGNED_OUT;

    //public static SceneManagment SceneManagment;

    public static SearchState SearchState { get; set; } = SearchState.NOT_SEARCHING;
    public static ILoggedClient LoggedClient { get; internal set; }
}