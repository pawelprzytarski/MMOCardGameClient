﻿using System.Collections;
using System.Collections.Generic;
using Backend.ClientCommunication;
using Backend.Main;
using UnityEngine;

public interface IBackendBuilder
{
    IMainController Build();
    IBackendBuilder SetUserName(string userName);
    IBackendBuilder SetCallbackObject(IClientCallback callback);
    bool ChangesOccurred();
}
