﻿public interface IAllCardsCollection
{
	CardScriptable GetCardById(int id);
}
