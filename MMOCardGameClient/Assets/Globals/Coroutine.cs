﻿using System;
using System.Collections;
using UnityEngine;

namespace ExtensionMethods
{
    public static class MonoBehaviourExt
    {
        public static Coroutine<T> StartCoroutine<T>(this UnityEngine.MonoBehaviour obj, IEnumerator coroutine)
        {
            var coroutineObject = new Coroutine<T>();
            coroutineObject.coroutine = obj.StartCoroutine(coroutineObject.InternalRoutine(coroutine));
            return coroutineObject;
        }

        public static Coroutine StartCoroutine(this MonoBehaviour obj, IEnumerator coroutine,
            Action<Exception> exceptionCallback)
        {
            return obj.StartCoroutine(tryCoroutine(coroutine, exceptionCallback));
        }
        
        private static IEnumerator tryCoroutine(IEnumerator coroutine, Action<Exception> exeptionCallback)
        {
            while (true)
            {
                object current;
                try
                {
                    if (coroutine.MoveNext() == false) break;
                    current = coroutine.Current;
                }
                catch (Exception e)
                {
                    exeptionCallback(e);
                    yield break;
                }
    
                yield return current;
            }
        }
    }
    
    
    
    public class Coroutine<T>
    {
        public Coroutine coroutine;
        private Exception e;
        private T returnValue;

        public T ReturnValue
        {
            get
            {
                if (e != null) throw e;
                return returnValue;
            }
        }

        public IEnumerator InternalRoutine(IEnumerator coroutine)
        {
            while (true)
            {
                try
                {
                    if (!coroutine.MoveNext()) yield break;
                }
                catch (Exception e)
                {
                    this.e = e;
                    yield break;
                }

                if (coroutine.Current != null && coroutine.Current.GetType() == typeof(T))
                {
                    returnValue = (T) coroutine.Current;
                    yield break;
                }

                yield return coroutine.Current;
            }
        }
    }
}
