﻿using System.Text;
using UnityEngine.Networking;

public abstract class RestRequestBuilder
{
    public readonly UnityWebRequest WebRequest;

    protected RestRequestBuilder(string requestMethod)
    {
        WebRequest = new UnityWebRequest() {method = requestMethod};
    }

    protected void addUploadHandler(string jsonString)
    {
        WebRequest.uploadHandler = new UploadHandlerRaw(Encoding.ASCII.GetBytes(jsonString))
            {contentType = @"application/json"};
    }

    protected void addDownloadHandler()
    {
        WebRequest.downloadHandler = new DownloadHandlerBuffer();
    }

    protected void addAuthX_TokenHeader()
    {
        WebRequest.SetRequestHeader("X-Auth-Token", UserInfo.AuthenticationToken);
    }

    protected StringBuilder buildDataServerUserUrl(string userName = null)
    {
        var builder = new StringBuilder();
        builder.Append(UserInfo.DataServerURL);
        builder.Append(UserInfo.UsersUrlTail);
        builder.Append("/");
        builder.Append(userName ?? UserInfo.UserName);
        return builder;
    }
}
