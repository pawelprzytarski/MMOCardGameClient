﻿using System;
using System.Collections.Generic;
using Backend.Constants;
using Backend.JSON.Entities;
using Backend.JSON.Readers;

public class NamedAbilities
{
    private Dictionary<int, NamedEffectJSON> namedAbilities=new Dictionary<int, NamedEffectJSON>();
    
    public NamedAbilities()
    {
        foreach (var ability in NamedEffectsJSONReader.GetNamedEffects(StaticPaths.NamedAbilitiesJsonDirectory))
        {
            namedAbilities.Add(ability.id, ability);
        }
    }

    public NamedEffectJSON getNamedAbility(int id)
    {
        return namedAbilities[id];
    }
}