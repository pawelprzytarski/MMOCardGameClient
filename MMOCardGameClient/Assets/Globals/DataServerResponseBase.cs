﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DataServerResponseBase
{
    public bool Success { get; set; }
    public string DownloadedText { get; }
    public long ResponseCode { get; }

    protected readonly string error;
    protected readonly bool isNetworkError;
    protected readonly bool isDone;

    protected readonly string requestName;

    protected string message;
    protected string defaultMessage;

    public DataServerResponseBase(UnityWebRequest webRequest, string requestName)
    {
        ResponseCode = webRequest.responseCode;
        error = webRequest.error;
        isNetworkError = webRequest.isNetworkError;
        isDone = webRequest.isDone;
        this.requestName = requestName;
        defaultMessage = UserInfo.getLocalizedString("UnknownError");
        if (webRequest.downloadHandler != null)
            DownloadedText = webRequest.downloadHandler.text;
    }

    protected string getDebugErrorString(string comment = null)
    {
        var builder = new StringBuilder();
        apendRequestName(builder);
        builder.Append(ResponseCode.ToString());
        appendError(builder);
        appendComment(comment, builder);

        return builder.ToString();
    }

    private static void appendComment(string comment, StringBuilder builder)
    {
        if (!string.IsNullOrEmpty(comment))
        {
            builder.Append(" [");
            builder.Append(comment);
            builder.Append("]");
        }
    }

    private void appendError(StringBuilder builder)
    {
        if (!string.IsNullOrEmpty(error))
        {
            builder.Append(" - ");
            builder.Append(error);
        }
    }

    private void apendRequestName(StringBuilder builder)
    {
        if (!string.IsNullOrEmpty(requestName))
        {
            builder.Append(requestName);
            builder.Append(": ");
        }
        else
            builder.Append("Request: ");
    }


    protected bool checkForNetworkError()
    {
        if (isNetworkError)
            message = UserInfo.getLocalizedString("ConnectionError");
        return !isNetworkError;
    }

    public void getMessageForUser(ref Text UserInfoText)
    {
        if (string.IsNullOrEmpty(message)) return;
        UserInfoText.text = message;
        UserInfoText.color = !Success ? Color.red : Color.white;
    }
}

public class NotSignedInException : Exception
{
    public NotSignedInException() { }
    public NotSignedInException(string Message) : base(Message) { }
    public NotSignedInException(string Message, Exception Inner) : base(Message, Inner) { }
}
