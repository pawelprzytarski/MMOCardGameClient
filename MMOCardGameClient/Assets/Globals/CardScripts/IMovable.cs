﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovable
{
	void SetParent(Transform parent);
	void moveToPosition(float x, float y);
	void resizeToSize(float width, float height);
	void SetDragable(bool enable);
}
