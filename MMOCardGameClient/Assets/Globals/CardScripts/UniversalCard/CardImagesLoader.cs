﻿using UnityEngine;
using System;
using System.Text;

public static class CardImagesLoader
{
	public static Sprite loadImage(int cardId)
	{
		var resourcePath = buildResourcePath(cardId);
		var loadedSprite = Resources.Load<Sprite>(resourcePath);
		if(loadedSprite != null)
			return loadedSprite;
		else
			throw new Exception("CardImagesLoader: " + resourcePath + " could not be found!");
	}

    public static Sprite loadDefaultImage()
    {
        return Resources.Load<Sprite>("Sprites/Cards/Test");
    }

	private static string buildResourcePath(int cardId)
	{
        var resourcePathBuilder = new StringBuilder();
	    resourcePathBuilder.Append(@"Sprites/Cards/");
	    resourcePathBuilder.Append(cardId.ToString());

		return resourcePathBuilder.ToString();
	}
}
