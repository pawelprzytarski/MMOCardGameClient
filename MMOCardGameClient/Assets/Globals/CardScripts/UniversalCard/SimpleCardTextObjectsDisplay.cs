﻿using System.Collections;
using System.Collections.Generic;
using Backend.GameInformation.Enums;
using Backend.JSON.Entities;
using UnityEngine;
using UnityEngine.UI;

public class SimpleCardTextObjectsDisplay : MonoBehaviour, IPlayfieldItemConfiguration
{
	public Text nameText;
	public Text descriptionText;

	public Text BattlefieldAttackText;
	public Text BattlefieldDefenceText;
	public Text BattlefieldOverloadText;

	public Text AttackDefenceText;

	public ScriptableObject Original { get; private set; }

	public void setUp(ScriptableObject item)
	{
		var card = item as CardScriptable;
		Original = card;
		
		if (card.cardType == CardType.Unknown) return;
		nameText.text = card.Name;
		string descriptionString = card.Description;
		if (card.JsonCard.activatedAbilities != null)
		{
			if (card.JsonCard.activatedAbilities.Length > 0)
				descriptionString += "\n\n";
			foreach (var ability in card.JsonCard.activatedAbilities)
			{
				descriptionString += appendCostString(ability.cost.neutral, "#c0c0c0ff");
				descriptionString += appendCostString(ability.cost.raiders, "#ff0000ff");
				descriptionString += appendCostString(ability.cost.scanners, "#00ffffff");
				descriptionString += appendCostString(ability.cost.whiteHats, "#ffffffff");
				descriptionString += appendCostString(ability.cost.psychotronnics, "#008000ff");
				descriptionString +=  "<color=\"#c0c0c0ff\">[" + ability.cost.overLoad + "]</color> ";
				descriptionString += "\n" + ability.description + "\n";
			}
		}

		descriptionText.text = descriptionString;
		if(AttackDefenceText != null)
			AttackDefenceText.text = card.Attack + "/" + card.Defence;
		
		if (BattlefieldAttackText != null)
			BattlefieldAttackText.text = card.Attack.ToString();
		if (BattlefieldDefenceText != null)
			BattlefieldDefenceText.text = card.Defence.ToString();
		if (BattlefieldOverloadText != null)
			BattlefieldOverloadText.text = card.overloadPoints.ToString();
	}

	private static string appendCostString(int cost, string color)
	{
		if (cost > 0)
			return "<color=\"" + color + "\">" + cost + "</color> ";
		return "";
	}
}
