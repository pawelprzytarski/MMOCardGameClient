﻿using Backend.GameInformation;
using Backend.GameInformation.Enums;
using JSON.Entities.CardReader;
using NUnit.Framework.Constraints;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Card", menuName = "Cards/Card")]
[System.Serializable]
public class CardScriptable : ScriptableObject
{
    public Card BackendCard;
    public CardJSON JsonCard;

    public string Name => JsonCard.name;
    public string Description { get; set; }
    public int InstanceId => BackendCard.InstanceId;
    public int? CardId => BackendCard.CardId;
    public CardCosts Cost => BackendCard.CardCosts;
    public int? Attack => BackendCard.Attack;
    public int? Defence => BackendCard.Defence;
    public CardType cardType => BackendCard.Type;
    public CardSubType subType => BackendCard.Subtype;
    public string cardClass => BackendCard.ClassValue;
    public string subClass => BackendCard.SubClass;
    public int? overloadPoints => BackendCard.OverloadedPoints;
    public int Amount = 0;
    
    public Sprite Artwork;
}