﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CardSelect : MonoBehaviour
{
	public Animator animator;
	
	void Awake () 
	{
		if(animator == null)
			Debug.Log("CardSelectForSwapEffect: animator not initialized! Selection animation will not play");
		try
		{
			IsSelected = false;
		}
		catch (Exception e)
		{
			Debug.Log("CardSelectForSwapEffect: " + e.Message);
		}
	}

	public bool IsSelected
	{
		get => animator != null && animator.GetBool("Selected");
		set => animator.SetBool("Selected", value);
	}

	public void ToggleSelect()
	{
		if (animator == null) return;
		IsSelected = !IsSelected;
	}

	public void Select()
	{
		if (animator == null) return;
		IsSelected = true;
	}

	public void Deselect()
	{
		if (animator == null) return;
		IsSelected = false;
	}

	public void DisableSelection()
	{
		animator = null;
	}
}
