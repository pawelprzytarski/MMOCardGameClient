﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardMoving : MonoBehaviour
{
	[SerializeField] private RectTransform invisibleCard;
	[SerializeField] private RectTransform mainCard;
	[SerializeField] private float movingSpeed = 1.0f;
 	
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		moveMainCardToInvisibleCard();
	}

	private void moveMainCardToInvisibleCard()
	{
		var destination = invisibleCard.position;
		var cardPosition = mainCard.position;
		var delta = destination - cardPosition;

		var movingVector = new Vector3(Mathf.Floor(delta.normalized.x * movingSpeed), 
			Mathf.Floor(delta.normalized.y * movingSpeed));
		var animationMove = movingVector * Time.deltaTime;
		
		if(animationMove.magnitude < delta.magnitude)
			mainCard.transform.Translate(animationMove);
		else
			mainCard.transform.Translate(delta);
	}

	public void MoveTo(Vector2 position)
	{
		invisibleCard.position = position;
	}
}
