﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace DataClient
{
    internal class HttpLoggedClient:ILoggedClient
    {
        public Action UnauthorizedAction { get; set; }
        private LoggonToken token;
        private String username;

        public HttpLoggedClient(LoggonToken token, String username)
        {
            this.token = token;
            this.username = username;
            UserInfo.AuthenticationToken = token.token;//dodano dla kompatybilności z zapytaniami korzystającymi z builderów. wyrzucić, gdy buildery znikną
        }

        public Request<Void> logout()
        {
            return appendStandardDataToRequest(new DeleteHttpRequest<Void>
            {
                Url = UserInfo.DataServerUrlAuthenticated
            });
        }

        public Request<GameEndData> getLastGameResult()
        {
            return appendStandardDataToRequest(new GetHttpRequest<GameEndData>
            {
                Url = UserInfo.DataServerUrlUsers+ "/" + username + UserInfo.GameResultUrlTail
            });
        }

        public Request<UserDeckJSON[]> getUserDecks()
        {
            return appendStandardDataToRequest(new GetHttpRequest<UserDeckJSON[]>()
            {
                SpecialDeserializer = DeserializeDecks,
                Url = UserInfo.DataServerUrlUsers + "/" + username + "/decks"
            });
        }

        UserDeckJSON[] DeserializeDecks(string arg)
        {
            return JsonConvert.DeserializeObject<UserDeckJSON[]>(arg);
        }

        public Request<Void> addCardToDeck(string deckName, int id)
        {
            return appendStandardDataToRequest(new PostHttpRequest<Void>()
            {
                Data = JsonConvert.SerializeObject(new CardId(){id = id}),
                Url = UserInfo.DataServerUrlUsers + "/" + username + "/decks/" + deckName
            });
        }

        public Request<Void> addCardsToDeck(string deckName, IList<UserCardCollection> cards)
        {
            return appendStandardDataToRequest(new PostHttpRequest<Void>()
            {
                Data = JsonConvert.SerializeObject(cards),
                Url = UserInfo.DataServerUrlUsers + "/" + username + "/decks/" + deckName + "/cards"
            });
        }

        public Request<Void> createNewDeck(string name)
        {
            return appendStandardDataToRequest(new PostHttpRequest<Void>()
            {
                Data = JsonConvert.SerializeObject(new DeckNameJSON() {name = name}),
                Url = UserInfo.DataServerUrlUsers + "/" + username + "/decks"
            });
        }

        public Request<Void> deleteDeck(string deckName)
        {
            return appendStandardDataToRequest(new DeleteHttpRequest<Void>()
            {
                Url = UserInfo.DataServerUrlUsers + "/" + username + "/decks/" + deckName
            });
        }

        public Request<Void> deleteCardsFromDeck(string deckName, IList<UserCardCollection> cards)
        {
            var data = JsonConvert.SerializeObject(cards);
            return appendStandardDataToRequest(new DeleteHttpRequestWithBody<Void>()
            {
                Data = data,
                Url = UserInfo.DataServerUrlUsers + "/" + username + "/decks/" + deckName + "/cards"
            });
        }
        
        public Request<UserCardCollection[]> getUserCardCollection()
        {
            return appendStandardDataToRequest(new GetHttpRequest<UserCardCollection[]>()
            {
                SpecialDeserializer = DeserializeCollection,
                Url = UserInfo.DataServerUrlUsers + "/" + username + "/cards"
            });
        }

        public Request<UserCardCollection[]> getCardsInDeck(string deckName)
        {
            return appendStandardDataToRequest(new GetHttpRequest<UserCardCollection[]>()
            {
                SpecialDeserializer = DeserializeCollection,
                Url = UserInfo.DataServerUrlUsers + "/" + username + "/decks/" + deckName
            });
        }

        private UserCardCollection[] DeserializeCollection(string arg)
        {
            return JsonConvert.DeserializeObject<UserCardCollection[]>(arg);
        }

        private Request<T> appendStandardDataToRequest<T>(HttpRequest<T> httpRequest)
        {
            httpRequest.AuthToken = token.token;
            if (UnauthorizedAction != null)
                httpRequest.UnauthorizedAction += UnauthorizedAction;
            return httpRequest;
        }
    }
}