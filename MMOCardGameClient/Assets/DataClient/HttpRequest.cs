﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace DataClient
{
    [Serializable]
    class ListOfErros{
        public List<String> errors;
    }

    public abstract class HttpRequest<T> : Request<T>
    {
        public string AuthToken { protected get; set; } = null;
        public string Url { protected get; set; }
        public Action UnauthorizedAction { get; set; }
        protected readonly string method;
        protected UnityWebRequest webRequest;

        protected HttpRequest(string method)
        {
            this.method = method;
        }

        public override void Send()
        {
            prepareWebRequest();
            webRequest.method = method;
            setRequestProperties();

            SendRequest();
        }

        protected void SendRequest()
        {
            webRequest.SendWebRequest().completed += obj =>
              {
                  if (webRequest.responseCode==0 && webRequest.error != null)
                    ErrorCallback?.Invoke(new List<string>{webRequest.error});
                  else if (webRequest.responseCode < 300 && webRequest.responseCode >= 200)
                      invokeSuccessMethod();
                else if (webRequest.responseCode == 401)
                  {
                      UnauthorizedAction?.Invoke();
                      ErrorCallback?.Invoke(new List<string> {"Unauthorized"});
                  }
                  else if (webRequest.responseCode == 403)
                  {
                      ForbiddenAction?.Invoke();
                      ErrorCallback?.Invoke(new List<string> {"Forbidden"});
                  }
                  else if (webRequest.responseCode < 500 && webRequest.responseCode >= 400)
                      invokeErrorMethodWithServerResponse();
                  else
                      invokeErrorMethodWithRequestError();
              };
        }

        private void invokeErrorMethodWithRequestError()
        {
            ErrorCallback?.Invoke(new List<string> { "HttpError: " + webRequest.responseCode });
        }

        private void invokeErrorMethodWithServerResponse()
        {
            var downloadedText = webRequest.downloadHandler.text;
            try
            {
                var listOfErrors = JsonUtility.FromJson<ListOfErros>("{ \"errors\":"+downloadedText+"}");
                ErrorCallback?.Invoke(listOfErrors.errors);
            }catch(Exception){
                ErrorCallback?.Invoke(new List<string> { "Cannot convert response to array of errors", downloadedText });
            }
        }

        private void invokeSuccessMethod()
        {
            if (typeof(T) == typeof(Void))
                SuccessCallback?.Invoke(default(T));
            else
            {
                var downloadedText = webRequest.downloadHandler.text;
                T result = deserializeObject(downloadedText);
                SuccessCallback?.Invoke(result);
            }
        }

        protected T deserializeObject(string downloadedText)
        {
            T result;
            if (SpecialDeserializer == null)
                result = JsonUtility.FromJson<T>(downloadedText);
            else result = SpecialDeserializer.Invoke(downloadedText);
            return result;
        }

        protected abstract void setRequestProperties();

        protected void prepareWebRequest()
        {
            webRequest = new UnityWebRequest
            {
                url = Url
            };
            addAuthX_TokenHeader();
            addDownloadHandler();
        }

        protected void setJsonToSend(string jsonString)
        {
            webRequest.uploadHandler = new UploadHandlerRaw(Encoding.ASCII.GetBytes(jsonString))
            { contentType = @"application/json" };
        }

        protected void addDownloadHandler()
        {
            webRequest.downloadHandler = new DownloadHandlerBuffer();
        }

        protected void addAuthX_TokenHeader()
        {
            if (AuthToken != null)
                webRequest.SetRequestHeader("X-Auth-Token", AuthToken);
        }
    }

    public abstract class UploadHttpRequest<T> : HttpRequest<T>
    {
        protected UploadHttpRequest(string method): base(method)
        {
        }

        public string Data { protected get; set; }

        protected override void setRequestProperties()
        {
            setJsonToSend(Data);
        }
    }

    public class PutHttpRequest<T> : UploadHttpRequest<T>
    {
        public PutHttpRequest() : base(UnityWebRequest.kHttpVerbPUT)
        {
        }
    }

    public class PostHttpRequest<T> : UploadHttpRequest<T>
    {
        public PostHttpRequest() : base(UnityWebRequest.kHttpVerbPOST)
        {
        }
    }

    public class GetHttpRequest<T> : HttpRequest<T>
    {
        public GetHttpRequest() : base(UnityWebRequest.kHttpVerbGET)
        {
        }

        protected override void setRequestProperties()
        {
        }
    }

    public class DeleteHttpRequest<T> : HttpRequest<T>
    {
        public DeleteHttpRequest() : base(UnityWebRequest.kHttpVerbDELETE)
        {
        }

        protected override void setRequestProperties()
        {
        }
    }

    public class DeleteHttpRequestWithBody<T> : UploadHttpRequest<T>
    {
        public DeleteHttpRequestWithBody() : base(UnityWebRequest.kHttpVerbDELETE)
        {
        }
    }

    public class LoginHttpRequest<T>: HttpRequest<T>{
        public string Username { protected get; set; }
        public string Password { protected get; set; }

        public LoginHttpRequest() : base(UnityWebRequest.kHttpVerbPOST)
        {
        }

        protected override void setRequestProperties()
        {
            webRequest.SetRequestHeader("Authorization", new LoginDataConverterScript(Username, Password).convertLoginDataToBase64LoginString());
        }
    }
}
