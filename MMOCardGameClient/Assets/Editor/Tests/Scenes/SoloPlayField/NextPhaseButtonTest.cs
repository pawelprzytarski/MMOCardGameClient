﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Threading.Tasks;
using Backend.Main;
using NSubstitute;
using ProtoBuffers;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NextPhaseEndedTest
{

    [UnitySetUp]
    public void loadScene()
    {
        SceneManager.LoadScene("SoloPlayfield");
    }

    [UnityTearDown]
    public void unloadScene()
    {
        SceneManager.UnloadSceneAsync("SoloPlayfield");
    }

    [Test]
    public void TestClickNextPhaseButton_shouldInvokeEndPhase()
    {
        var controller = Substitute.For<IMainController>();

        var gameObject = GameObject.Find("NextPhazeButton");
        Button nextPhaseButton = gameObject.GetComponent<Button>();
        nextPhaseButton.GetComponentInChildren<NextPhaseButtonScript>().setBackendController(controller); //set mock

        nextPhaseButton.onClick.Invoke();

        controller.Received().EndPhase();
    }

    [UnityTest]
    public IEnumerator TestCallbackListenersEvent_otherPhaseSamePlayer_changePhaseTextAndSetButtonActive()
    {
        var gameObject = GameObject.Find("NextPhazeButton");
        Button nextPhaseButton = gameObject.GetComponent<Button>();

        CallbackListeners.get().OnPhaseEndedEvent(0, 0, GamePhase.Attack, GameInfo.PlayerId);
        nextPhaseButton.GetComponentInChildren<NextPhaseButtonScript>().Update();

        var buttonText = nextPhaseButton.GetComponentInChildren<Text>().text;
        Assert.AreEqual("EndPhase.Attack", buttonText);
        Assert.True(nextPhaseButton.interactable);
        yield return null;
    }

    [UnityTest]
    public IEnumerator TestCallbackListenersEvent_otherPhaseOtherPlayer_changePhaseTextAndSetButtonInactive()
    {
        var gameObject = GameObject.Find("NextPhazeButton");
        Button nextPhaseButton = gameObject.GetComponent<Button>();

        CallbackListeners.get().OnPhaseEndedEvent(0, 0, GamePhase.Attack, GameInfo.PlayerId+1);
        nextPhaseButton.GetComponentInChildren<NextPhaseButtonScript>().Update();

        var buttonText = nextPhaseButton.GetComponentInChildren<Text>().text;
        Assert.AreEqual("EndPhase.Attack", buttonText);
        Assert.False(nextPhaseButton.interactable);
        yield return null;
    }
}