﻿using Localization;
using NUnit.Framework;
using UnityEngine;

namespace Tests.Localization
{
    public class LocalizationLoaderTests
    {
        [Test]
        public void loadLocalizationFor_nonExistingLanguage_loadDefaultLanguage()
        {
            var localizationLoader = new LocalizationLoader(Application.dataPath);
            var dictionary = localizationLoader.loadLocalizationFor("nonExistingLanguage");
            Assert.AreEqual("yes", dictionary.getLocalizedTextIfExists("default"));
        }

        [Test]
        public void loadLocalizationFor_PlLanguage_loadPlLanguage()
        {
            var localizationLoader = new LocalizationLoader(Application.dataPath);
            var dictionary = localizationLoader.loadLocalizationFor("pl-PL");
            Assert.AreEqual("Polski", dictionary.getLocalizedTextIfExists("language"));
        }
    }
}