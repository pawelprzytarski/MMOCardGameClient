﻿using System;
using Backend.ClientCommunication;
using Backend.Main;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace Tests.Backend
{
	public class BackendInitializationTests 
	{
		[Test]
		public void buildMainController_noParametersGiven_throwsException()
		{
			var builder = new MainControllerBuilder();
			Assert.Throws<Exception>(() => builder.Build());
		}

		[Test]
		public void buildMainController_onlyUserNameGiven_throwsException()
		{
			var builder = new MainControllerBuilder();
			builder.SetUserName("someUser");
			
			Assert.Throws<Exception>(() => builder.Build());
		}

		[Test]
		public void buildMainController_onlyCallbackObjectGiven_throwsException()
		{
			var builder = new MainControllerBuilder();
			var callbackMock = Substitute.For<IClientCallback>();
			builder.SetCallbackObject(callbackMock);

			Assert.Throws<Exception>(() => builder.Build());
		}

		[Test]
		public void buildMainController_giveAllNecessaryParams_builderShouldReturnMainController()
		{
			const string sampleUserName = "someUser";
			var callbackMock = Substitute.For<IClientCallback>();
			var mainController = new MainController(sampleUserName, callbackMock, 
				Application.dataPath + "/StreamingAssets/CardsJson/cards.json",
				Application.dataPath + "/StreamingAssets/Logs");
			var builder = new MainControllerBuilder();

			builder.SetUserName(sampleUserName);
			builder.SetCallbackObject(callbackMock);
			var builtController = builder.Build();
			
			Assert.Equals(mainController.GetType(), builtController.GetType());
		}
		
		[Test]
		public void backendControllerBuildsAndReturnsMainController()
		{
			var mainControllerMock = Substitute.For<IMainController>();
			
			var builderMock = Substitute.For<IBackendBuilder>();
			builderMock.Build().Returns(mainControllerMock);;
			builderMock.ChangesOccurred().Returns(true);

			var backendController = new GameObject().AddComponent<BackendController>();
			backendController.BackendBuilder = builderMock;
			var returnedMainController = backendController.Controller;
			
			mainControllerMock.NotifyThatPreparedForGame();
			Assert.AreEqual(mainControllerMock, returnedMainController);
		}
		
		
	
	}

}


