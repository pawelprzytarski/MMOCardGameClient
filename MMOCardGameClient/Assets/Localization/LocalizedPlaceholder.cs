﻿using UnityEngine.UI;

namespace Localization
{
    public class LocalizedPlaceholder : LocalizedText
    {
        private void Start()
        {
            var text = GetComponentInChildren<Text>();
            text.text = UserInfo.getLocalizedString(text.text);
        }
    }
}