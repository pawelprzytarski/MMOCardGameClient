﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Localization
{
    public class LocalizationLoader
    {
        private readonly string defaultLanguage = "pl-PL";
        private readonly string fileLandExt = ".json";
        private readonly string langFilePath;

        public LocalizationLoader()
        {
            langFilePath = Application.dataPath + @"/StreamingAssets/langs/lang_";
        }

        public LocalizationLoader(string langFilePath)
        {
            this.langFilePath = langFilePath + @"/StreamingAssets/langs/lang_";
        }

        public LocalizationDictionary loadLocalizationFor(string language)
        {
            var filePath = langFilePath + language + fileLandExt;
            if (!File.Exists(filePath))
            {
                filePath = langFilePath + defaultLanguage + fileLandExt;
                language = defaultLanguage;
                Debug.Log("Cannot find language: " + language);
            }

            if (!File.Exists(filePath))
            {
                Debug.Log("Cannot find default language. Path: " + filePath);
                return new LocalizationDictionary(new Dictionary<string, string>());
            }

            var json = File.ReadAllText(filePath);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            Debug.Log("Loaded language: "+language);
            return new LocalizationDictionary(dictionary);
        }
    }
}