﻿using System.Collections.Generic;
using System.Linq;

namespace Backend.GameInformation {
    public class Player {
        public Player(string name, int id, int heroId, int defence, int cardsInLibraryCount) {
            Name = name;
            Id = id;
            HeroId = heroId;
            Defence = defence;
            CardsInLibraryCount = cardsInLibraryCount;

            Library = new Dictionary<int, Card>();
            Cemetery = new Dictionary<int, Card>();
            Hand = new Dictionary<int, Card>();
            BattleField = new Dictionary<int, Card>();
            RemovedCards = new Dictionary<int, Card>();

            LinkPoints = new LinkPoints(0, 0, 0, 0, 0);
            CardsInHandCount = 0;
        }

        public int Defence { get; set; }
        public int HeroId { get; }
        public int Id { get; }
        public int CardsInLibraryCount { get; set; }
        public int CardsInHandCount { get; set; }

        public string Name { get; }

        public Dictionary<int, Card> Library { get; set; }
        public Dictionary<int, Card> Cemetery { get; set; }
        public Dictionary<int, Card> Hand { get; set; }
        public Dictionary<int, Card> BattleField { get; set; }
        public Dictionary<int, Card> RemovedCards { get; set; }

        public LinkPoints LinkPoints { get; set; }

        protected bool Equals(Player other) {
            return Defence == other.Defence && HeroId == other.HeroId && Id == other.Id &&
                   CardsInLibraryCount == other.CardsInLibraryCount &&
                   CardsInHandCount == other.CardsInHandCount && string.Equals(Name, other.Name) &&
                   Library.SequenceEqual(other.Library) && Cemetery.SequenceEqual(other.Cemetery) &&
                   Hand.SequenceEqual(other.Hand) && BattleField.SequenceEqual(other.BattleField) &&
                   RemovedCards.SequenceEqual(other.RemovedCards) && Equals(LinkPoints, other.LinkPoints);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Player) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Defence;
                hashCode = (hashCode * 397) ^ HeroId;
                hashCode = (hashCode * 397) ^ Id;
                hashCode = (hashCode * 397) ^ CardsInLibraryCount;
                hashCode = (hashCode * 397) ^ CardsInHandCount;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Library != null ? Library.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Cemetery != null ? Cemetery.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Hand != null ? Hand.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (BattleField != null ? BattleField.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (RemovedCards != null ? RemovedCards.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LinkPoints != null ? LinkPoints.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}