﻿using ProtoBuffers;

namespace Backend.GameInformation {
    public class Target {
        public string Description { get; set; }
        public int Id { get; set; }
        public TargetType TargetType { get; set; }
    }
}