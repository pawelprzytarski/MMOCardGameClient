﻿namespace Backend.GameInformation {
    public class LinkPoints {
        public LinkPoints(int psychos, int raiders, int scanners, int whiteHats, int neutral) {
            Psychos = psychos;
            Raiders = raiders;
            Scanners = scanners;
            WhiteHats = whiteHats;
            Neutral = neutral;
        }

        public int Psychos { get; set; }
        public int Raiders { get; set; }
        public int Scanners { get; set; }
        public int WhiteHats { get; set; }
        public int Neutral { get; set; }

        protected bool Equals(LinkPoints other) {
            return Psychos == other.Psychos && Raiders == other.Raiders && Scanners == other.Scanners &&
                   WhiteHats == other.WhiteHats && Neutral == other.Neutral;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((LinkPoints) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Psychos;
                hashCode = (hashCode * 397) ^ Raiders;
                hashCode = (hashCode * 397) ^ Scanners;
                hashCode = (hashCode * 397) ^ WhiteHats;
                hashCode = (hashCode * 397) ^ Neutral;
                return hashCode;
            }
        }
    }
}