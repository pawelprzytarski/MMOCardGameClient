﻿namespace Backend.GameInformation.Enums {
    public enum CardSubType {
        Basic,
        Legendary,
        Unknown
    }
}