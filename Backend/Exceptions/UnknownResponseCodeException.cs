﻿using System;

namespace Backend.Exceptions {
    internal class UnknownResponseCodeException : Exception { }
}