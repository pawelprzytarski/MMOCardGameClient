﻿using System;

namespace Backend.Exceptions {
    internal class WrongTypeNameException : Exception { }
}