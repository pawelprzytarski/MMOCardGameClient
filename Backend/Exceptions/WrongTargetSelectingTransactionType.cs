﻿using System;

namespace Backend.Exceptions {
    internal class WrongTargetSelectingTransactionType : Exception { }
}