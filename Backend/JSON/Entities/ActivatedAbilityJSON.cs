﻿using Newtonsoft.Json;

namespace Backend.JSON.Entities {
    public class ActivatedAbilityJSON {
        [JsonConstructor]
        public ActivatedAbilityJSON(int id, string description, AbilityCostJSON cost) {
            this.id = id;
            this.description = description;
            this.cost = cost;
        }

        public int id { get; }
        public string description { get; }
        public AbilityCostJSON cost { get; }

        protected bool Equals(ActivatedAbilityJSON other) {
            return id == other.id && string.Equals(description, other.description) && Equals(cost, other.cost);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ActivatedAbilityJSON) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = id;
                hashCode = (hashCode * 397) ^ (description != null ? description.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (cost != null ? cost.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}