﻿using System.IO;

using Backend.JSON.Entities;

using Newtonsoft.Json;

namespace Backend.JSON.Readers {
    public class NamedEffectsJSONReader {
        public static NamedEffectJSON[] GetNamedEffects(string path) {
            var serializer = new JsonSerializer();

            using (var sw = new StreamReader(path))
            using (JsonReader reader = new JsonTextReader(sw)) {
                return serializer.Deserialize<NamedEffectJSON[]>(reader);
            }
        }
    }
}