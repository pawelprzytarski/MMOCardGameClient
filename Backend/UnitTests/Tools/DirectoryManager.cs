﻿using System.IO;

using NUnit.Framework;

namespace UnitTests.Tools {
    public class DirectoryManager {
        public static string getTestDir() {
            return Path.GetDirectoryName(Path.GetDirectoryName(TestContext.CurrentContext.TestDirectory));
        }
    }
}