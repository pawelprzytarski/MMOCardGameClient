﻿using System.Collections.Generic;
using System.Threading;

using Backend.ClientCommunication;
using Backend.Constants;
using Backend.GameInformation;
using Backend.GRPCCommunication;

using ProtoBuffers;

namespace Backend.Main {
    public class MainController : IMainController {
        private readonly GameState gameState;
        private readonly MessageManager messageManager;
        private readonly MessagesHandler messagesHandler;

        public MainController(string userName, IClientCallback callback, string cardJSONLocation, string logLocation) {
            Directories.CardJSONLocation = cardJSONLocation;
            Directories.LogLocation = logLocation;
            gameState = new GameState(userName);
            messagesHandler = new MessagesHandler(callback, gameState);
            messageManager = new MessageManager(userName, messagesHandler, gameState);
        }

        /*Client calls this method after creating MainController to make connection with server*/
        public void MakeConnection(string address, string token) {
            messageManager.MakeConnection(address, token);
        }

        /*Client calls this method after preaparing for game after connection is made*/
        public void NotifyThatPreparedForGame() {
            new Thread(() => {
                messageManager.StartReceiveingThread();
                messageManager.SendEndCurrentPhaseMessage();
            }).Start();
        }

        /*Client calls this method to accept hand as it is*/
        public void AcceptHand() {
            new Thread(() => { messageManager.SendEndCurrentPhaseMessage(); }).Start();
        }

        /*Client calls this method to replace choosen cards
         client can call this method only once*/
        public void ReplaceCards(List<int> replacedCards) {
            new Thread(() => { messageManager.ReplaceCards(replacedCards); }).Start();
        }

        /*Client calls this method to make mulligan
         client can call this method as long as he has cards in hand*/
        public void Mulligan() {
            new Thread(() => { messageManager.Mulligan(); }).Start();
        }

        /*Client calls this method to end phase*/
        public void EndPhase() {
            new Thread(() => { messageManager.SendEndCurrentPhaseMessage(); }).Start();
        }

        /*Client calls this method to play card from hand
         After this method client has to go through transaction
         (even if it will only tell that you don't need transaction)
         To really play the card client will need to call FinishPlay method after getting YouFinishedChoosing callback*/
        public void PlayCard(int cardInstanceId) {
            new Thread(() => { messageManager.PlayCard(cardInstanceId); }).Start();
        }

        /*The same as PlayCard, but used for playing card abilities*/
        public void UseCardAbility(int cardInstanceId) {
            new Thread(() => { messageManager.UseCardAbility(cardInstanceId); }).Start();
        }

        /*Client calls this method to Select options after PickOptions callback*/
        public void SelectOptions(int transactionId, int instanceId, List<Target> selectedOptions) {
            new Thread(() => { messageManager.SelectOptions(transactionId, instanceId, selectedOptions); }).Start();
        }

        /*Client calls this method to really play card/ablility after getting YouFinishedChoosing callback*/
        public void FinishPlay(int transactionId, int instanceId) {
            new Thread(() => { messageManager.FinishPlay(transactionId, instanceId); }).Start();
        }

        /*Client calls this method if he wants to abort playing card/ability*/
        public void AbortPlay(int transactionId, int instanceId) {
            new Thread(() => { messageManager.AbortPlay(transactionId, instanceId); }).Start();
        }

        /*Client calls this method if he doesn't want to react to stack*/
        public void PassStackReaction() {
            new Thread(() => { messageManager.SendEndCurrentPhaseMessage(); }).Start();
        }

        /*Client calls this method if he wants to surrender*/
        public void Surrender() {
            new Thread(() => { messageManager.Surrender(); }).Start();
        }

        /*Client calls this method if he wants to choose targets for his attacker
         he needs to remember data from YouFinishedChoosing callback
         to be able to really choose attackers*/
        public void ChooseAttackersTargets(int cardInstanceId) {
            new Thread(() => { messageManager.ChooseAttackersTargets(cardInstanceId); }).Start();
        }

        /*Client calls this method if he wants to choose targets for his defender
         he needs to remember data from YouFinishedChoosing callback
         to be able to really choose defenders*/
        public void ChooseDefendersTargets(int cardInstanceId) {
            new Thread(() => { messageManager.ChooseDefendersTargets(cardInstanceId); }).Start();
        }

        /*Client calls this method to really attack/defend
         selectedTargets is dictionary od <instanceId, transactionId>*/
        public void AttackDefend(Dictionary<int, int> selectedTargets) {
            new Thread(() => { messageManager.AttackDefend(selectedTargets); }).Start();
        }

        /*Client calls this method to end connection after game*/
        public void CancelConnection() {
            messageManager.CancelConnection();
        }

        /*Client calls this method to accept trigger transaction*/
        public void AcceptTrigger(int transactionId, int instanceId, TriggerType triggerType)
        {
            new Thread(() => { messageManager.AcceptTrigger(transactionId, instanceId, triggerType); }).Start();
        }
    }
}