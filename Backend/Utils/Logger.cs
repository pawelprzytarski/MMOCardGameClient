﻿using System;
using System.IO;

using Backend.Constants;

namespace Backend.Utils {
    public class Logger {
        public static void LogException(Exception e) {
            try {
                File.AppendAllText(Directories.LogLocation,
                    Environment.NewLine + DateTime.Now + Environment.NewLine + e.Message + Environment.NewLine +
                    e.StackTrace + Environment.NewLine);
            }
            catch (Exception ex) {
                Directory.CreateDirectory(Path.GetDirectoryName(Directories.CriticalLogLocation));
                File.AppendAllText(Directories.CriticalLogLocation,
                    Environment.NewLine + DateTime.Now + Environment.NewLine + ex.Message + Environment.NewLine +
                    ex.StackTrace + Environment.NewLine);
            }
        }

        public static void LogMessage(string message) {
            try {
                File.AppendAllText(Directories.MessagesLocation,
                    Environment.NewLine + DateTime.Now + Environment.NewLine + message + Environment.NewLine);
            }
            catch (Exception ex) {
                Directory.CreateDirectory(Path.GetDirectoryName(Directories.CriticalLogLocation));
                File.AppendAllText(Directories.CriticalLogLocation,
                    Environment.NewLine + DateTime.Now + Environment.NewLine + ex.Message + Environment.NewLine +
                    ex.StackTrace + Environment.NewLine);
            }
        }
    }
}